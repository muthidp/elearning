﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Core;
using Core.DTO.Lms;
using LMS.Service.Authentication;
using LMS.Service.Lms.Class;
using LMS.Service.Lms.Course;
using LMS.Service.Lms.Instructor;
using LMS.Service.Lms.Lesson;
using LMS.Service.Md.FileUpload;
using LMS.Service.System.Institution;
using LMS.Service.System.ModuleGroup;
using LMS.Service.System.User;
using LMS.Service.System.Role;

namespace LMS.Service
{
    public class Dependency
    {
        public static ContainerBuilder Register(ContainerBuilder builder)
        {
            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>().InstancePerRequest();
            builder.RegisterType<InstitutionService>().As<IInstitutionService>().InstancePerRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<ModuleGroupService>().As<IModuleGroupService>().InstancePerRequest();

            builder.RegisterType<CourseService>().As<ICourseService>().InstancePerRequest();
            builder.RegisterType<CourseValidator>().As<IValidator<CourseModel>>().InstancePerRequest();
            builder.RegisterType<ClassService>().As<IClassService>().InstancePerRequest();
            builder.RegisterType<ClassValidator>().As<IValidator<ClassModel>>().InstancePerRequest();
            builder.RegisterType<LessonService>().As<ILessonService>().InstancePerRequest();
            builder.RegisterType<LessonValidator>().As<IValidator<LessonModel>>().InstancePerRequest();

            builder.RegisterType<FileUploadService>().As<IFileUploadService>().InstancePerRequest();

            builder.RegisterType<InstructorService>().As<IInstructorService>().InstancePerRequest();
            
            builder.RegisterType<RoleService>().As<IRoleService>().InstancePerRequest();



            return builder;
        }
    }
}
