﻿using Autofac;
using Core;
using DAL;

namespace LMS.Service
{
    public class ServiceConfig
    {
        public static void RegisterRepositories(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();
            builder.RegisterType<EfUnitOfWork>().As(typeof(IUnitOfWork)).InstancePerRequest();
            builder.RegisterType<eLearning_Database>();
            builder.RegisterType<SpWrapper>().As(typeof (ISpWrapper)).InstancePerRequest();
        }
    }
}
