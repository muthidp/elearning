﻿using System;
using System.Linq;
using System.Linq.Dynamic;
using Core;
using LinqKit;

namespace LMS.Service
{
    public class BaseQueryExpression<T> where T : BaseEntityModel 
    {
        public static IQueryable<T> QueryResult(IQueryable<T> data, ref BaseQueryModel queryModel)
        {
            var dataSearch = DefaultSearchQueryable(data, ref queryModel);
            var dataSort = DefaultSortQueryable(dataSearch, ref queryModel);
            var dataPagination = DefaultPaginateQueryable(dataSort, ref queryModel);
            return dataPagination;
        }

        public static IQueryable<T> DefaultSearchQueryable(IQueryable<T> data, ref BaseQueryModel searchQuery)
        {
            var query = "";
            if (searchQuery.Search == null)
            {
                searchQuery.TotalData = data.Count();
                searchQuery.TotalPage = GetTotalPage(searchQuery.TotalData, searchQuery.RowPerPage);
                return data;
            }
            if (String.IsNullOrEmpty(searchQuery.Search.Keyword))
            {
                searchQuery.TotalData = data.Count();
                searchQuery.TotalPage = GetTotalPage(searchQuery.TotalData, searchQuery.RowPerPage);
                return data;
            }
            var model = searchQuery;
            model.Search.Fields.ForEach(f =>
            {
                query = query + String.Format("{0}.ToString().ToLower().Contains(\"{1}\")", f, model.Search.Keyword.ToLower());
                query = query + " || ";
            });

            query = query.Substring(0, query.Length - 4);

            var searchExpression = query;
            var result = !String.IsNullOrEmpty(searchExpression) ? data.Where(searchExpression) : data;
            searchQuery.TotalData = result.Count();
            searchQuery.TotalPage = GetTotalPage(searchQuery.TotalData, searchQuery.RowPerPage);
            return result;
        }

        public static IQueryable<T> DefaultSortQueryable(IQueryable<T> data, ref BaseQueryModel searchQuery)
        {
            if (!String.IsNullOrEmpty(searchQuery.SortBy))
            {
                return searchQuery.IsSortAsc ? data.OrderBy(searchQuery.SortBy + " ASC") : data.OrderBy(searchQuery.SortBy + " DESC");
            }
            return data;
        }

        public static IQueryable<T> DefaultPaginateQueryable(IQueryable<T> data, ref BaseQueryModel searchQuery)
        {
            var take = searchQuery.RowPerPage;
            var skip = (searchQuery.Page - 1) * take;
            return data.Skip(skip).Take(take);
        }

        private static int GetTotalPage(int totalData, int rowPerPage)
        {
            return (totalData / rowPerPage) + 1;
        }
    }
}
