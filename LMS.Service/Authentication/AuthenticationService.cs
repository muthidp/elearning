﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using Core;
using Core.DTO.Sys;
using Core.Helper;
using DAL.Database.Sys;

namespace LMS.Service.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IRepository<sys_user> _userRepository;

        public AuthenticationService(
            IRepository<sys_user> userRepository)
        {
            _userRepository = userRepository;
        }

        public IOperationInfo Authenticate(string username, string password)
        {
            var info = new OperationInfo();
            try
            {
                var user =
                    _userRepository.Query()
                        .FirstOrDefault(u =>
                            (u.Username.ToLower().Equals(username.ToLower()) || u.Email.ToLower().Equals(username.ToLower())) &&
                            u.IsActive &&
                            !u.Deactivated);

                if (user == null)
                {
                    info.validations.Add("username", "Username not found.");
                }
                else
                {
                    if (user.Password != password.GetMd5Hash())
                    {
                        info.validations.Add("username", "Wrong password.");
                    }
                    else
                    {
                        FormsAuthentication.SetAuthCookie(username, false);
                        var userModel = user.TransformTo<UserModel>();
                        info.AttachData("user", user.TransformTo<UserModel>());
                        info.Success();
                    }
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public bool IsLogin()
        {
            var user = HttpContext.Current.User.Identity.Name;
            return !String.IsNullOrEmpty(user);
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}
