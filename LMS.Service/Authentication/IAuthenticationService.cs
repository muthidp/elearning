﻿using Core;

namespace LMS.Service.Authentication
{
    public interface IAuthenticationService
    {
        IOperationInfo Authenticate(string username, string password);
        bool IsLogin();
        void Logout();
    }
}
