﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;

namespace LMS.Service.Lms.Course
{
    public interface ICourseService
    {
        List<CourseModel> GetAllWithBaseQuery(long? institutionId, ref BaseQueryModel queryModel);
        List<CourseModel> GetAll(long? institutionId);
        IOperationInfo Save(CourseModel dataModel);
        IOperationInfo Delete(long courseId);
    }
}
