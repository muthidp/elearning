﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;
using DAL.Database.Lms;

namespace LMS.Service.Lms.Course
{
    public class CourseValidator : BaseValidator<CourseModel>
    {
        private readonly IRepository<lms_course> _courseRepository;

        public CourseValidator(
            IRepository<lms_course> courseRepository )
        {
            _courseRepository = courseRepository;
        }

        public override IOperationInfo Validate(CourseModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.CourseCode))
            {
                info.AttachValidation("CourseCode", "Course code cannot be empty");
            }
            if (String.IsNullOrEmpty(dataModel.CourseLabel))
            {
                info.AttachValidation("CourseLabel", "Course label cannot be empty");
            }

            if (info.Valid())
            {
                var existingCourse = _courseRepository.Query()
                    .FirstOrDefault(e => e.CourseCode == dataModel.CourseCode && e.IsActive);

                if (existingCourse != null && existingCourse.CourseId != dataModel.CourseId)
                {
                    info.AttachValidation("Duplicate", "Course code " + dataModel.CourseCode + " already exist.");
                }
            }

            return info;
        }
    }
}
