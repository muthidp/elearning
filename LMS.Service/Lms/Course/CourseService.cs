﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;
using DAL.Database.Lms;
using LMS.Service;

namespace LMS.Service.Lms.Course
{
    public class CourseService : ICourseService
    {
        private readonly IRepository<lms_course> _courseRepository;
        private readonly IValidator<CourseModel> _courseValidator;

        public CourseService(
            IRepository<lms_course> courseRepository,
            IValidator<CourseModel> courseValidator )
        {
            _courseRepository = courseRepository;
            _courseValidator = courseValidator;
        }

        public List<CourseModel> GetAllWithBaseQuery(long? institutionId, ref BaseQueryModel queryModel)
        {
            var courses = _courseRepository.Query()
                .Where(e => e.InstitutionId == institutionId &&
                            e.IsActive)
                .QueryResult(ref queryModel)
                .ToList();

            return courses.Select(e => e.TransformTo<CourseModel>()).ToList();
        }

        public List<CourseModel> GetAll(long? institutionId)
        {
            var courses = _courseRepository.Query()
                .Where(e => e.InstitutionId == institutionId &&
                            e.IsActive)
                .ToList();
            return courses.Select(e => e.TransformTo<CourseModel>()).ToList();
        }

        public IOperationInfo Save(CourseModel dataModel)
        {
            var info = _courseValidator.GenerateOperationInfo();
            try
            {
                info = _courseValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var course = dataModel.CourseId > 0
                        ? _courseRepository.Query().First(e => e.CourseId == dataModel.CourseId)
                        : new lms_course();

                    course.CourseCode = dataModel.CourseCode;
                    course.CourseLabel = dataModel.CourseLabel;
                    course.Description = dataModel.Description;
                    course.YearIntake = dataModel.YearIntake;
                    course.InstitutionId = dataModel.InstitutionId;
                    course.PictureUrl = dataModel.PictureUrl;

                    _courseRepository.Save(course);
                    _courseRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long courseId)
        {
            var info = new OperationInfo();
            try
            {
                var course = _courseRepository.Query().FirstOrDefault(e => e.CourseId == courseId);
                if (course != null)
                {
                    _courseRepository.SoftDelete(course);
                    _courseRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }
    }
}
