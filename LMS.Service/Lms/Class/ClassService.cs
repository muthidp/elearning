﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;
using Core.DTO.Student;
using DAL.Database.Lms;

namespace LMS.Service.Lms.Class
{
    public class ClassService : IClassService
    {
        private readonly IRepository<lms_class> _classRepository;
        private readonly IValidator<ClassModel> _classValidator;
        private readonly IRepository<lms_student_class> _studentClassRepository;

        public ClassService(
            IRepository<lms_class> classRepository,
            IValidator<ClassModel> classValidator,
            IRepository<lms_student_class> studentClassRepository )
        {
            _classRepository = classRepository;
            _classValidator = classValidator;
            _studentClassRepository = studentClassRepository;
        }

        public List<ClassModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var classes = _classRepository.Query()
                .Where(e => e.IsActive);
            var resultQuery = BaseQueryExpression<lms_class>.QueryResult(classes, ref queryModel);

            return resultQuery.ToList().Select(BuildClassModel).ToList();
        }

        public List<ClassModel> GetAll()
        {
            var classes = _classRepository.Query()
                .Where(e => e.IsActive)
                .ToList();
            return classes.Select(BuildClassModel).ToList();
        }

        private ClassModel BuildClassModel(lms_class _class)
        {
            var classModel = _class.TransformTo<ClassModel>();
            classModel.CourseLabel = _class.Course != null
                ? _class.Course.CourseLabel
                : "";
            classModel.InstructorName = _class.Instructor != null
                ? _class.Instructor.Username
                : "";
            classModel.CoInstructorName = _class.CoInstructor != null
                ? _class.CoInstructor.Username
                : "";
            return classModel;
        }

        public IOperationInfo Save(ClassModel dataModel)
        {
            var info = _classValidator.GenerateOperationInfo();
            try
            {
                info = _classValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var _class = dataModel.ClassId > 0
                        ? _classRepository.Query().First(e => e.ClassId == dataModel.ClassId)
                        : new lms_class();

                    _class.ClassCode = dataModel.ClassCode;
                    _class.ClassLabel = dataModel.ClassLabel;
                    _class.CourseId = dataModel.CourseId;
                    _class.InstructorId = dataModel.InstructorId;
                    _class.CoInstructorId = dataModel.CoInstructorId;

                    _classRepository.Save(_class);
                    _classRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long classId)
        {
            var info = new OperationInfo();
            try
            {
                var _class = _classRepository.Query().FirstOrDefault(e => e.ClassId == classId);
                if (_class != null)
                {
                    _classRepository.SoftDelete(_class);
                    _classRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public List<StudentModel> GetStudentsUnderClass(long? classId)
        {
            var studentClasses = _studentClassRepository.Query()
                .Where(e => e.IsActive && e.ClassId == classId)
                .ToList();

            var result = new List<StudentModel>();
            studentClasses.ForEach(sc =>
            {
                var s = sc.Student.TransformTo<StudentModel>();
                s.StudentClassId = sc.StudentClassId;
                s.Name = sc.Student.Username;
                result.Add(s);
            });

            return result;
        }

        public IOperationInfo AddStudentToClass(long? userId, long? classId)
        {
            var info = new OperationInfo();
            try
            {
                if (!(userId > 0 && classId > 0))
                {
                    info.AttachValidation("data", "data not valid.");
                    return info;
                }

                var data =
                    _studentClassRepository.Query()
                        .FirstOrDefault(e => e.IsActive && e.UserId == userId && e.ClassId == classId);

                if (data != null)
                {
                    info.AttachValidation("user", "user already added to the class before.");
                    return info;
                }

                data = new lms_student_class()
                {
                    UserId = userId,
                    ClassId = classId
                };
                _studentClassRepository.Save(data);
                _studentClassRepository.Commit();

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo DeleteStudentFromClass(long? studentClassId)
        {
            var info = new OperationInfo();
            try
            {
                var data = _studentClassRepository.Query().FirstOrDefault(e => e.StudentClassId == studentClassId);
                if (data != null)
                {
                    _studentClassRepository.SoftDelete(data);
                    _studentClassRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }
    }
}
