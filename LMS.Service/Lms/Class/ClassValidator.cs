﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;
using DAL.Database.Lms;

namespace LMS.Service.Lms.Class
{
    public class ClassValidator : BaseValidator<ClassModel>
    {
        private readonly IRepository<lms_class> _classRepository;

        public ClassValidator(
            IRepository<lms_class> classRepository)
        {
            _classRepository = classRepository;
        }

        public override IOperationInfo Validate(ClassModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.ClassCode))
            {
                info.AttachValidation("ClassCode", "Class code cannot be empty");
            }
            if (String.IsNullOrEmpty(dataModel.ClassLabel))
            {
                info.AttachValidation("ClassLabel", "Class label cannot be empty");
            }
            if (dataModel.InstructorId == null || dataModel.InstructorId == 0)
            {
                info.AttachValidation("Instructor", "Please set instructor for the class");
            }
            if (dataModel.InstructorId > 0 && dataModel.CoInstructorId > 0)
            {
                info.AttachValidation("CoInstructor", "Co-instructor and instructor cannot be the same");
            }

            if (info.Valid())
            {
                var existingCourse = _classRepository.Query()
                    .FirstOrDefault(e => e.ClassCode == dataModel.ClassCode && e.IsActive);

                if (existingCourse != null && existingCourse.ClassId != dataModel.ClassId)
                {
                    info.AttachValidation("Duplicate", "Class code " + dataModel.ClassCode + " already exist.");
                }
            }

            return info;
        }
    }
}
