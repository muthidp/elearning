﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;
using Core.DTO.Student;

namespace LMS.Service.Lms.Class
{
    public interface IClassService
    {
        List<ClassModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<ClassModel> GetAll();
        IOperationInfo Save(ClassModel dataModel);
        IOperationInfo Delete(long courseId);
        List<StudentModel> GetStudentsUnderClass(long? classId);
        IOperationInfo AddStudentToClass(long? userId, long? classId);
        IOperationInfo DeleteStudentFromClass(long? studentClassId);
    }
}
