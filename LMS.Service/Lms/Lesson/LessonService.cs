﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;
using DAL.Database.Lms;

namespace LMS.Service.Lms.Lesson
{
    public class LessonService : ILessonService
    {
        private readonly IRepository<lms_lesson> _lessonRepository;
        private readonly IValidator<LessonModel> _lessonValidator;

        public LessonService(
            IRepository<lms_lesson> lessonRepository,
            IValidator<LessonModel> lessonValidator )
        {
            _lessonRepository = lessonRepository;
            _lessonValidator = lessonValidator;
        }

        public List<LessonModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var lessons = _lessonRepository.Query()
                .Where(e => e.IsActive);
            var resultQuery = BaseQueryExpression<lms_lesson>.QueryResult(lessons, ref queryModel);

            return resultQuery.ToList().Select(BuildLessonModel).ToList();
        }

        public List<LessonModel> GetAll()
        {
            var lessons = _lessonRepository.Query()
                .Where(e => e.IsActive)
                .ToList();
            return lessons.Select(BuildLessonModel).ToList();
        }

        private LessonModel BuildLessonModel(lms_lesson lesson)
        {
            var lessonModel = lesson.TransformTo<LessonModel>();
            lessonModel.ClassLabel = lesson.Class != null
                ? lesson.Class.ClassLabel
                : "";
            return lessonModel;
        }

        public IOperationInfo Save(LessonModel dataModel)
        {
            var info = _lessonValidator.GenerateOperationInfo();
            try
            {
                info = _lessonValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var lesson = dataModel.LessonId > 0
                        ? _lessonRepository.Query().First(e => e.LessonId == dataModel.LessonId)
                        : new lms_lesson();

                    lesson.LessonCode = dataModel.LessonCode;
                    lesson.LessonLabel = dataModel.LessonLabel;
                    lesson.ClassId = dataModel.ClassId;

                    _lessonRepository.Save(lesson);
                    _lessonRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long lessonId)
        {
            var info = new OperationInfo();
            try
            {
                var lesson = _lessonRepository.Query().FirstOrDefault(e => e.LessonId == lessonId);
                if (lesson != null)
                {
                    _lessonRepository.SoftDelete(lesson);
                    _lessonRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }
    }
}
