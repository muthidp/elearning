﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;
using DAL.Database.Lms;

namespace LMS.Service.Lms.Class
{
    public class LessonValidator : BaseValidator<LessonModel>
    {
        private readonly IRepository<lms_lesson> _lessonRepository;

        public LessonValidator(
            IRepository<lms_lesson> lessonRepository)
        {
            _lessonRepository = lessonRepository;
        }

        public override IOperationInfo Validate(LessonModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.LessonCode))
            {
                info.AttachValidation("LessonCode", "Lesson code cannot be empty");
            }
            if (String.IsNullOrEmpty(dataModel.LessonLabel))
            {
                info.AttachValidation("LessonLabel", "Lesson label cannot be empty");
            }

            if (info.Valid())
            {
                var existingCourse = _lessonRepository.Query()
                    .FirstOrDefault(e => e.LessonCode == dataModel.LessonCode && e.IsActive);

                if (existingCourse != null && existingCourse.LessonId != dataModel.LessonId)
                {
                    info.AttachValidation("Duplicate", "Lesson code " + dataModel.LessonCode + " already exist.");
                }
            }

            return info;
        }
    }
}
