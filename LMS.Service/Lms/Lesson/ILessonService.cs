﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;

namespace LMS.Service.Lms.Lesson
{
    public interface ILessonService
    {
        List<LessonModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<LessonModel> GetAll();
        IOperationInfo Save(LessonModel dataModel);
        IOperationInfo Delete(long lessonId);
    }
}
