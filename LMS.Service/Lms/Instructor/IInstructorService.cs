﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.DTO.Lms;
using Core.DTO.Student;

namespace LMS.Service.Lms.Instructor
{
    public interface IInstructorService
    {
        List<CourseModel> GetInstructorCourses(long? userId);
        List<ClassModel> GetInstructorClasses(long? userId, long? courseId);
    }
}
