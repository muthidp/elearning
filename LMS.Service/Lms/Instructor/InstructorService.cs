﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Lms;
using Core.DTO.Student;
using Core.DTO.Sys;
using DAL.Database.Lms;
using LMS.Service.System.User;

namespace LMS.Service.Lms.Instructor
{
    public class InstructorService : IInstructorService
    {
        private readonly IRepository<lms_course> _courseRepository;
        private readonly IRepository<lms_class> _classRepository;
        private readonly IRepository<lms_student_class> _studentClassRepository;
        private readonly IUserService _userService;

        public InstructorService(
            IRepository<lms_course> courseRepository,
            IRepository<lms_class> classRepository,
            IRepository<lms_student_class> studentClassRepository, 
            IUserService userService)
        {
            _courseRepository = courseRepository;
            _classRepository = classRepository;
            _studentClassRepository = studentClassRepository;
            _userService = userService;
        }

        public List<CourseModel> GetInstructorCourses(long? userId)
        {
            var classes =
                _classRepository.Query()
                    .Where(e => e.IsActive && (e.InstructorId == userId || e.CoInstructorId == userId))
                    .ToList();

            var result = new List<CourseModel>();
            classes
                .GroupBy(e=> e.Course.CourseId).Select(e=> e.FirstOrDefault())
                .OrderBy(e=> e.Course.CourseLabel)
                .ToList()
                .ForEach(e =>
                {
                    result.Add(e.Course.TransformTo<CourseModel>());
                });
            return result;
        }

        public List<ClassModel> GetInstructorClasses(long? userId, long? courseId)
        {
            var classes =
                _classRepository.Query()
                    .Where(
                        e =>
                            e.IsActive && (e.InstructorId == userId || e.CoInstructorId == userId) &&
                            e.CourseId == courseId)
                    .ToList();

            return classes.Select(e => e.TransformTo<ClassModel>()).ToList();
        }

        
    }
}
