﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace LMS.Service
{
    public abstract class BaseValidator<T> : IValidator<T>
    {
        public abstract IOperationInfo Validate(T dataModel, IOperationInfo info);

        public IValidationInfo GenerateValidationInfo()
        {
            return new ValidationInfo();
        }

        public IOperationInfo GenerateOperationInfo()
        {
            return new OperationInfo();
        }
    }
}
