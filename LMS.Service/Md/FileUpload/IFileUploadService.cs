﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Md;

namespace LMS.Service.Md.FileUpload
{
    public interface IFileUploadService
    {
        IOperationInfo Upload(FileUploadModel model);
    }
}
