﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Core;
using Core.DTO.Md;
using Core.Enum;
using DAL.Database.Md;
using DAL.Database.Sys;
using iTextSharp.awt.geom;

namespace LMS.Service.Md.FileUpload
{
    public class FileUploadService : IFileUploadService
    {
        private readonly IRepository<md_media_descriptor> _mediaDescriptorRepository;
        private readonly IRepository<sys_application_setting> _applicationSettingRepository;
        private readonly IRepository<md_media_asset> _mediaAssetRepository;

        public FileUploadService(
            IRepository<md_media_descriptor> mediaDescriptorRepository,
            IRepository<sys_application_setting> applicationSettingRepository,
            IRepository<md_media_asset> mediaAssetRepository)
        {
            _mediaDescriptorRepository = mediaDescriptorRepository;
            _applicationSettingRepository = applicationSettingRepository;
            _mediaAssetRepository = mediaAssetRepository;
        }

        private MediaType _mediaType = MediaType.Nothing;
        private HttpPostedFileBase _fileUploaded;
        private FileUploadModel _model;
        private md_media_descriptor _mediaDescriptor;

        public IOperationInfo Upload(FileUploadModel model)
        {
            var info = new OperationInfo();
            BeforeHandle(model);
            Handle(ref info);
            AfterHandle(); ;

            return info;
        }

        public bool UploadValidate()
        {
            return _mediaType != MediaType.Nothing;
        }

        public void BeforeHandle(FileUploadModel model)
        {
            _model = model;
            _fileUploaded = model.FileUploaded;
            if (_fileUploaded == null || _fileUploaded.ContentLength <= 0) return;

            var md = _mediaDescriptorRepository.Query()
                .FirstOrDefault(e => e.MimeTypeDefinition.ToLower() == _fileUploaded.ContentType);

            if (md != null)
            {
                _mediaDescriptor = md;
                _mediaType = md.MediaType;
            }
        }

        public IOperationInfo Handle(ref OperationInfo info)
        {
            info = new OperationInfo();

            string headerAcceptVariable = HttpContext.Current.Request.Headers["ACCEPT"];
            //Delegate.Response.StatusCode = 200;
            HttpContext.Current.Request.ContentType = "text/plain";
            HttpContext.Current.Response.Clear();

            if (UploadValidate())
            {
                try
                {
                    var fileBucketDirectory = GetFileBucketPath();
                    var fileDirectory = _mediaType.ToString() + "/" + _fileUploaded.ContentType;

                    var resolved = ResolveInBucket(fileBucketDirectory, fileDirectory);
                    var filename = _fileUploaded.FileName;

                    if (filename.Contains("\\"))
                    {
                        var names = filename.Split('\\');
                        filename = names.Last();
                    }

                    filename = DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + filename;

                    var directoryAndFileName = Path.Combine(resolved, filename);
                    _fileUploaded.SaveAs(directoryAndFileName);

                    var ma = new md_media_asset();
                    ma.InstitutionId = _model.InstitutionId;
                    ma.MediaDescriptorId = _mediaDescriptor.MediaDescriptorId;
                    ma.AbsolutePath = directoryAndFileName;
                    ma.RelativePath = directoryAndFileName;
                    ma.Title = filename;
                    ma.Description = filename;
                    ma.Filename = filename;

                    var rp = ma.RelativePath.Replace(HttpContext.Current.Server.MapPath("~"), "");
                    ma.RelativePath = Path.Combine("\\", rp);

                    _mediaAssetRepository.Save(ma);
                    _mediaAssetRepository.Commit();

                    info.datas.Add("file", ma);

                    info.Success();
                }
                catch (Exception ex)
                {
                    info.ExceptionOccurred().AttachException(ex);
                }
            }
            else
                info.validations.Add("file_type", "File type not supported.");

            return info;
        }

        public void AfterHandle()
        {

        }

        public string GetFileBucketPath()
        {
            var appSetting = _applicationSettingRepository.Query()
                .FirstOrDefault(e => e.KeyValue.Equals("FILEBUCKET") && e.IsActive);

            return appSetting != null ? appSetting.SettingValueDefault : HttpContext.Current.Server.MapPath("~/Filebucket");
        }

        public string ResolveInBucket(string fileBucketDirectory, string fileDirectory)
        {
            if (String.IsNullOrEmpty(fileBucketDirectory))
            {
                fileDirectory = HttpContext.Current.Server.MapPath("~Filebucket");
            }

            var path = fileBucketDirectory;
            fileDirectory
            .Split('/')
            .Where(k => !String.IsNullOrEmpty(k) && !String.IsNullOrWhiteSpace(k))
            .ToList()
            .ForEach(k =>
            {
                var dirpath = Path.Combine(path, k);
                if (!Directory.Exists(dirpath))
                {
                    try
                    {
                        Directory.CreateDirectory(dirpath);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                path = dirpath;
            });
            return path;
        }
    }
}
