﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core;
using Core.DTO.Sys;
using DAL.Database.Sys;
using System;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Core.Helper;

namespace LMS.Service.System.User
{
    public class UserService : IUserService
    {
        private readonly IRepository<sys_user> _userRepository;
        private readonly IRepository<sys_role_acl> _roleAclRepository;
        private readonly IRepository<sys_role> _roleRepo;
        private readonly IRepository<sys_institution> _institutionRepo;

        public UserService(
            IRepository<sys_user> userRepository,
            IRepository<sys_role_acl> roleAclRepository,
            IRepository<sys_role> roleRepo,
            IRepository<sys_institution> institutionRepo)
        {
            _userRepository = userRepository;
            _roleAclRepository = roleAclRepository;
            _roleRepo = roleRepo;
            _institutionRepo = institutionRepo;
        }

        public UserInfoModel GetCurrentUser()
        {
            var username = HttpContext.Current.User.Identity.Name;
            var user = _userRepository.Query().FirstOrDefault(e => e.IsActive && (e.Username.Equals(username) || e.Email.Equals(username)));
            if (user == null) return null;

            return BuildUserInfo(user);
        }

        public UserInfoModel BuildUserInfo(sys_user user)
        {
            if (user == null) return null;
            var model = user.TransformTo<UserInfoModel>();
            model.DefaultRoute = user.Role != null ? user.Role.DefaultRoute : "";

            model.Role = user.Role.TransformTo<RoleModel>();
            model.Institution = user.Institution.TransformTo<InstitutionModel>();
            model.Acls = new List<string>();

            if (model.Role != null)
            {
                var acls =
                    _roleAclRepository.Query()
                        .Where(e => e.RoleId == model.Role.RoleId && e.IsActive && e.AclId != null)
                        .Select(e => e.Acl.Key)
                        .ToList();
                model.Acls = acls;
            }

            return model;
        }

        public UserInfoModel GetUserInfo(string username)
        {
            var user = _userRepository.Query().FirstOrDefault(e => e.IsActive && e.Username.Equals(username));
            if (user == null) return null;

            return BuildUserInfo(user);
        }
        
        public List<UserInfoModel> GetUserInstructors(long? institutionId)
        {
            return GetUsersByRole(institutionId, Constant.Instructor);
        }

        public List<UserInfoModel> GetUserStudents(long? institutionId)
        {
            return GetUsersByRole(institutionId, Constant.Student);
        }

        public List<UserInfoModel> GetUsersByRole(long? institutionId, string role)
        {
            var users = _userRepository.Query()
                .Where(e => e.IsActive && e.Role.RoleCode.Equals(role))
                .ToList();

            return users.Select(BuildUserInfo).ToList();
        } 

        //For CRUD
        public List<UserInfoModel> GetAllWithBaseQuery(long? institutionId, ref BaseQueryModel queryModel)
        {
            var users = _userRepository.Query()
                .Where(e => e.InstitutionId == institutionId &&
                            e.IsActive)
                .QueryResult(ref queryModel)
                .ToList();


            List<UserInfoModel> userinfo = new List<UserInfoModel>();

            foreach (var u in users)
            {
                UserInfoModel user = u.TransformTo<UserInfoModel>();

                var role = _roleRepo.Query().Where(r => r.RoleId == user.RoleId
                    && r.IsActive).FirstOrDefault();

                user.Role = new RoleModel();
                if (role != null)
                    user.Role = role.TransformTo<RoleModel>(); user.RoleLabel = user.Role.RoleLabel;


                var institution = _institutionRepo.Query().Where(f => f.InstitutionId == user.InstitutionId
                   && f.IsActive).FirstOrDefault();


                user.Institution = new InstitutionModel();
                if (institution != null)
                    user.Institution = institution.TransformTo<InstitutionModel>(); user.InstitutionLabel = user.Institution.InstitutionLabel;

                userinfo.Add(user);
            }

            return userinfo;
        }

        public List<UserInfoModel> GetAll(long? institutionId)
        {
            var Users = _userRepository.Query()
                .Where(e => e.InstitutionId == institutionId &&
                            e.IsActive)
                .ToList();

            List<UserInfoModel> userinfo = new List<UserInfoModel>();

            Users.ForEach(u =>
            {
                UserInfoModel user = u.TransformTo<UserInfoModel>();

                var role = _roleRepo.Query().Where(r => r.RoleId == user.RoleId
                    && r.IsActive).FirstOrDefault();


                user.Role = new RoleModel();
                if (role != null)
                    user.Role = role.TransformTo<RoleModel>(); user.RoleLabel = user.Role.RoleLabel;

                var institution = _institutionRepo.Query().Where(f => f.InstitutionId == user.InstitutionId
                   && f.IsActive).FirstOrDefault();


                user.Institution = new InstitutionModel();
                if (institution != null)
                    user.Institution = institution.TransformTo<InstitutionModel>(); user.InstitutionLabel = user.Institution.InstitutionLabel;

                userinfo.Add(user);
            });

            return userinfo;
        }

        public IOperationInfo Save(UserModel dataModel)
        {
            var info = new OperationInfo();
            try
            {
                var user = dataModel.UserId > 0
                       ? _userRepository.Query().First(e => e.UserId == dataModel.UserId)
                       : new sys_user();

                user.UserId = dataModel.UserId;
                user.Username = dataModel.Username;
                user.Email = dataModel.Email;
                user.Password = dataModel.Password.GetMd5Hash();

                user.RoleId = dataModel.RoleId;
                user.InstitutionId = dataModel.InstitutionId;
                user.Deactivated = false;

                _userRepository.Save(user);
                _userRepository.Commit();

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long userId)
        {
            var info = new OperationInfo();
            try
            {
                var user = _userRepository.Query().FirstOrDefault(e => e.UserId == userId);
                if (user != null)
                {
                    _userRepository.SoftDelete(user);
                    _userRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public InstitutionUserLimitCheck CheckLimit(long InstitutionId)
        {
            var info = new InstitutionUserLimitCheck();
            info.result = false;

            var institution = _institutionRepo.Query().Where(f => f.InstitutionId == InstitutionId
                 && f.IsActive).FirstOrDefault();

            int no_of_user = _userRepository.Query().Where(e => e.InstitutionId == InstitutionId
                && !e.Deactivated && e.IsActive).Count();

            int limit = institution.UserLimit;

            if (limit > no_of_user)
                info.result = true;
            else
            {
                info.result = false;
                info.msg = "Number of users is limited to " + limit + " for " +
                       institution.InstitutionLabel + ". Please contact an administrator.";
            }

            return info;
        }

    }
}
