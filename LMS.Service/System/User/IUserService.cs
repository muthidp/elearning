﻿using System.Collections.Generic;
using Core.DTO.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Sys;

namespace LMS.Service.System.User
{
    public interface IUserService
    {
        UserInfoModel GetCurrentUser();
        UserInfoModel GetUserInfo(string username);
        List<UserInfoModel> GetUserInstructors(long? institutionId);
        List<UserInfoModel> GetUserStudents(long? institutionId);
        List<UserInfoModel> GetUsersByRole(long? institutionId, string role);

        List<UserInfoModel> GetAllWithBaseQuery(long? institutionId, ref BaseQueryModel queryModel);
        List<UserInfoModel> GetAll(long? institutionId);
        IOperationInfo Save(UserModel dataModel);
        IOperationInfo Delete(long UserId);

        InstitutionUserLimitCheck CheckLimit(long InstitutionId);

    }
}
