﻿using System.Collections.Generic;
using Core.DTO.Sys;

namespace LMS.Service.System.ModuleGroup
{
    public interface IModuleGroupService
    {
        List<ModuleGroupInfo> GetAllModuleGroups();
    }
}
