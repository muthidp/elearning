﻿using Core.DTO.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Sys;

namespace LMS.Service.System.Role
{
    public interface IRoleService
    {
        List<RoleModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<RoleModel> GetAll(long? institutionId);
        IOperationInfo Save(RoleModel dataModel);
        IOperationInfo Delete(long RoleId);
    }
}
