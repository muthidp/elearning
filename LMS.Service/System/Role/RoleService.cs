﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core;
using Core.DTO.Sys;
using DAL.Database.Sys;
using System;

namespace LMS.Service.System.Role
{
    public class RoleService : IRoleService
    {
        private readonly IRepository<sys_role> _RoleRepository;
        private readonly IRepository<sys_role_acl> _roleAclRepository;
        private readonly IRepository<sys_institution> _institutionRepo;

        public RoleService(
            IRepository<sys_role> RoleRepository,
            IRepository<sys_role_acl> roleAclRepository,
            IRepository<sys_institution> institutionRepo)
        {
            _RoleRepository = RoleRepository;
            _roleAclRepository = roleAclRepository;
            _institutionRepo = institutionRepo;
        }

        //For CRUD
        public List<RoleModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var Roles = _RoleRepository.Query()
                .Where(e => e.IsActive)
                .QueryResult(ref queryModel)
                .ToList();

            return Roles.ToList().Select(r => r.TransformTo<RoleModel>()).ToList();
        }

        public List<RoleModel> GetAll(long? institutionId)
        {
            var Roles = _RoleRepository.Query()
                .Where(e => e.IsActive)
                .ToList();

            return Roles.ToList().Select(r => r.TransformTo<RoleModel>()).ToList();
        }

        public IOperationInfo Save(RoleModel dataModel)
        {
            var info = new OperationInfo();
            try
            {
                var Role = dataModel.RoleId > 0
                       ? _RoleRepository.Query().First(e => e.RoleId == dataModel.RoleId)
                       : new sys_role();

                Role.RoleId = dataModel.RoleId;
                Role.RoleCode = dataModel.RoleCode;
                Role.RoleLabel = dataModel.RoleLabel;
                Role.DefaultRoute = dataModel.DefaultRoute;
                _RoleRepository.Save(Role);
                _RoleRepository.Commit();

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long RoleId)
        {
            var info = new OperationInfo();
            try
            {
                var Role = _RoleRepository.Query().FirstOrDefault(e => e.RoleId == RoleId);
                if (Role != null)
                {
                    _RoleRepository.SoftDelete(Role);
                    _RoleRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

    }
}
