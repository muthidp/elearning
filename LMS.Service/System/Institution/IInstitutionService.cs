﻿using System.Collections.Generic;
using Core.DTO.Sys;
using System.Linq;
using Core;
using DAL.Database.Sys;
using System.Web;
using System;

namespace LMS.Service.System.Institution
{
    public interface IInstitutionService
    {
        List<InstitutionModel> GetAll();
        InstitutionModel FindOne(string institutionCode);
        List<InstitutionModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<InstitutionModel> GetAll(long? institutionId);
        IOperationInfo Save(InstitutionModel dataModel);
        IOperationInfo Delete(long InstitutionId);
    }
}
