﻿using System.Collections.Generic;
using System.Linq;
using Core;
using Core.DTO.Sys;
using DAL.Database.Sys;
using System.Web;
using System;

namespace LMS.Service.System.Institution
{
    public class InstitutionService : IInstitutionService
    {
        private readonly IRepository<sys_institution> _institutionRepository;

        public InstitutionService(
            IRepository<sys_institution> institutionRepository )
        {
            _institutionRepository = institutionRepository;
        }

        public List<InstitutionModel> GetAll()
        {
            return _institutionRepository.Query().Where(e => e.IsActive)
                .ToList().Select(e => e.TransformTo<InstitutionModel>()).ToList();
        }

        public InstitutionModel FindOne(string institutionCode)
        {
            var ins =
                _institutionRepository.Query()
                    .FirstOrDefault(e => e.InstitutionCode.Equals(institutionCode) && e.IsActive);

            return ins != null ? ins.TransformTo<InstitutionModel>() : null;
        }


        //For CRUD
        public List<InstitutionModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var Institutions = _institutionRepository.Query()
                .Where(e => e.IsActive)
                .QueryResult(ref queryModel)
                .ToList();

            return Institutions.ToList().Select(r => r.TransformTo<InstitutionModel>()).ToList();
        }

        public List<InstitutionModel> GetAll(long? institutionId)
        {
            var Institutions = _institutionRepository.Query()
                .Where(e => e.IsActive)
                .ToList();

            return Institutions.ToList().Select(r => r.TransformTo<InstitutionModel>()).ToList();
        }

        public IOperationInfo Save(InstitutionModel dataModel)
        {
            var info = new OperationInfo();
            try
            {
                var Institution = dataModel.InstitutionId > 0
                       ? _institutionRepository.Query().First(e => e.InstitutionId == dataModel.InstitutionId)
                       : new sys_institution();

                Institution.InstitutionId = dataModel.InstitutionId;
                Institution.InstitutionCode = dataModel.InstitutionCode;
                Institution.InstitutionLabel = dataModel.InstitutionLabel;
                Institution.Subscribed = dataModel.Subscribed;
                Institution.UserLimit = dataModel.UserLimit;

                _institutionRepository.Save(Institution);
                _institutionRepository.Commit();

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long InstitutionId)
        {
            var info = new OperationInfo();
            try
            {
                var Institution = _institutionRepository.Query().FirstOrDefault(e => e.InstitutionId == InstitutionId);
                if (Institution != null)
                {
                    _institutionRepository.SoftDelete(Institution);
                    _institutionRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }


    }
}
