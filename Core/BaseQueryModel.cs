﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class BaseQueryModel
    {
        public int Page { get; set; }
        public int RowPerPage { get; set; }
        public string SortBy { get; set; }
        public bool IsSortAsc { get; set; }
        public int TotalData { get; set; }
        public int TotalPage { get; set; }
        public SearchModel Search { get; set; }

        public class SearchModel
        {
            public string Keyword { get; set; }
            public string[] Fields { get; set; }
        }
    }
}
