﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IValidator<T> : IInfoFactory
    {
        IOperationInfo Validate(T dataModel, IOperationInfo info);
    }
}
