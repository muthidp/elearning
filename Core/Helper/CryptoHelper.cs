﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Core.Helper
{
    public static class CryptoHelper
    {
        public static string GetMd5Hash(this string s)
        {
            using (MD5 MD5 = MD5.Create())
            {
                var input = Encoding.ASCII.GetBytes(s);
                var hashedInput = MD5.ComputeHash(input);

                var sb = new StringBuilder();
                for (var i = 0; i < hashedInput.Length; i++)
                {
                    sb.Append(hashedInput[i].ToString("X2"));
                }

                return sb.ToString();
            }
        }

        public static string RandomCharacter(this object o)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (var i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }
    }
}
