﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enum
{
    public enum MediaType
    {
        Nothing,
        Picture,
        Audio,
        HtmlScript,
        Video,
        Application,
        DataFormat,
        File
    }
}
