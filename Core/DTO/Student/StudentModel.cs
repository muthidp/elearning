﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Student
{
    public class StudentModel
    {
        public long? StudentClassId { get; set; }
        public long UserId { get; set; }
        public string Name { get; set; }
    }
}
