﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Sys
{
    public class ModuleGroupModel
    {
        public long ModuleGroupId { get; set; }
        public string Label { get; set; }
        public string Route { get; set; }
        public string AclKey { get; set; }
    }

    public class ModuleGroupInfo : ModuleGroupModel
    {
        public List<ModuleModel> Modules { get; set; } 
    }

    public class ModuleModel
    {
        public long ModuleId { get; set; }
        public string Label { get; set; }
        public string Route { get; set; }
        public string AclKey { get; set; }
    }
}
