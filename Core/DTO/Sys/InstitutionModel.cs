﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Sys
{
    public class InstitutionModel
    {
        public long InstitutionId { get; set; }
        public string InstitutionCode { get; set; }
        public string InstitutionLabel { get; set; }

        public bool Subscribed { get; set; }
        public int UserLimit { get; set; }
    }
}
