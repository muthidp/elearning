﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Sys
{
    public class RoleModel
    {
        public long RoleId { get; set; }
        public string RoleCode { get; set; }
        public string RoleLabel { get; set; }
        public string DefaultRoute { get; set; }
    }
}
