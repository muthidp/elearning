﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Lms
{
    public class LessonModel
    {
        public long LessonId { get; set; }
        public string LessonCode { get; set; }
        public string LessonLabel { get; set; }
        public long? ClassId { get; set; }
        public string ClassLabel { get; set; }
    }
}
