﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Lms
{
    public class ClassModel
    {
        public long ClassId { get; set; }
        public string ClassCode { get; set; }
        public string ClassLabel { get; set; }
        public long? CourseId { get; set; }
        public string CourseLabel { get; set; }
        public long? InstructorId { get; set; }
        public string InstructorName { get; set; }
        public long? CoInstructorId { get; set; }
        public string CoInstructorName { get; set; }
    }


}
