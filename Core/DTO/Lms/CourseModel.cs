﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Lms
{
    public class CourseModel
    {
        public long CourseId { get; set; }
        public string CourseCode { get; set; }
        public string CourseLabel { get; set; }
        public string Description { get; set; }
        public string YearIntake { get; set; }
        public long? InstitutionId { get; set; }
        public string PictureUrl { get; set; }
    }
}
