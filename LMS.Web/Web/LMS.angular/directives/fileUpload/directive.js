﻿angular.module('app')
    .directive('fileUploader', function () {
        return {
            restrict: 'EA',
            scope: {
                fileResult: '=',
                pickFn: '&'
            },
            replace: true,
            transclude: true,
            templateUrl: DIRECTIVE_PATH + 'fileUpload/template.html',
            controller: function ($scope, $rootScope, $session, fileUpload) {
                var userSession = $session.get();
                var defaultHandler = fileUpload.defaults;

                $scope.uploadConfig = {
                    url: '/FileUpload/Upload',
                    formData: {
                        InstitutionId: userSession.Institution.InstitutionId
                    },
                    add: function (e, data) {
                        $scope.queue = [];
                        $scope.preview = false;
                        $scope.previewSrc = '';

                        // Modify the state
                        defaultHandler.add(e, data);
                        data.submit();
                    },
                    done: function (e, data) {
                        var fileResult = data.result.data.datas.file;
                        $scope.fileResult = fileResult;
                        $scope.pickFn(
                            {
                                result: $scope.fileResult
                            });
                    }
                };
            },
            link: function (scope, element, attrs) {
            }
        };
    });