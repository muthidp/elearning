﻿angular.module('app')
       .controller('signUpController', ['$rootScope', '$scope', '$state', '$timeout', 'UserService',
           function ($rootScope, $scope, $state, $timeout, UserService) {

               $scope.a = 1;
               $scope.b = 2;

               $scope.user = {
                   name: '',
                   email: '',
                   password: '',
                   confirm_password: '',
                   role: 'student',
                   institution_id: $rootScope.Institution.institution_id
               };

               $scope.reset = function () {
                   $scope.user = {
                       name: '',
                       email: '',
                       password: '',
                       confirm_password: '',
                       role: 'student',
                       institution_id: $rootScope.Institution.institution_id
                   };
               };

               $scope.roles = [];

               $scope.roles.push({
                   title: 'Student',
                   value: 'student'
               });

               $scope.roles.push({
                   title: 'Teacher',
                   value: 'teacher'
               });

               $scope.roles.push({
                   title: 'Admin',
                   value: 'admin'
               });

               kk = $scope.roles;
               pp = $scope.namesss.text;


               $scope.validations = {
                   name: false,
                   email: false,
                   not_email: false,
                   password: false,
                   retype: false,
                   retype_not_matching: false
               }

               $scope.CreateAccount = function () {

                   var proceed = true;

                   if (isEmpty($scope.user.name)) {
                       $scope.validations.name = true;
                       $timeout(function () {
                           $scope.validations.name = false;
                       }, 5000);
                       proceed = false;
                   } else {
                       $scope.user.name = $scope.user.name.trim();
                   }

                   if (isEmpty($scope.user.email)) {
                       $scope.validations.email = true;
                       $timeout(function () {
                           $scope.validations.email = false;
                       }, 5000);
                       proceed = false;
                   } else {
                       $scope.user.email = $scope.user.email.trim();

                       if (!validateEmail($scope.user.email)) {
                           $scope.validations.not_email = true;
                           $timeout(function () {
                               $scope.validations.not_email = false;
                           }, 5000);
                           proceed = false;
                       }
                   }

                   if (isEmpty($scope.user.password)) {
                       $scope.validations.password = true;
                       $timeout(function () {
                           $scope.validations.password = false;
                       }, 5000);
                       proceed = false;
                   }

                   if (!isEmpty($scope.user.password)) {
                       if (isEmpty($scope.user.confirm_password)) {
                           $scope.validations.retype = true;
                           $timeout(function () {
                               $scope.validations.retype = false;
                           }, 5000);
                           proceed = false;
                       }
                   }

                   if ($scope.user.password != $scope.user.confirm_password) {
                       $scope.validations.retype_not_matching = true;
                       $timeout(function () {
                           $scope.validations.retype_not_matching = false;
                       }, 5000);
                       proceed = false;
                   }

                   if (proceed) {
                       //console.log("User to save ::", $scope.user);

                       var result = UserService.SignUpUser($scope.user, function () {

                           if (result.data.executed) {
                               $scope.reset();
                               $rootScope.clearAllInfo();
                               $rootScope.addInfo("success", "Registration successful.");
                               $timeout(function () {
                                   $rootScope.clearAllInfo();
                               }, 4000);

                           } else {
                               $scope.reset();
                               $rootScope.clearAllInfo();
                               $rootScope.addInfo("error", "Registration failed. Please contact an adminstrator.");
                               $timeout(function () {
                                   $rootScope.clearAllInfo();
                               }, 4000);
                           }

                       });

                   }
               };
   }]);