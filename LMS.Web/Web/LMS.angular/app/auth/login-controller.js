﻿angular.module('app')
       .controller('logInController', ['$rootScope', '$scope', '$state', '$timeout', 'AuthenticationService', '$location', '$session', 'UserService',
           function ($rootScope, $scope, $state, $timeout, authenticationService, $location, $session, userService) {
               $rootScope.showNavigation = false;

               $scope.user = {
                   email: null,
                   password: null
               };

               var getUserInfo = function(username) {
                   var json = userService.GetUserInfo({
                       username: username
                   }, function() {
                       $session.set(json.data);
                   });
               };

               console.log("SCOPEE ", $scope);
               $scope.LogIn = function () {
                   $rootScope.addInfo("warning", "Loggin in...");
                   var jsonLogin = authenticationService.Login({
                       email: $scope.user.email,
                       password: $scope.user.password
                   }, function() {
                       var data = jsonLogin.data;
                       if (data.success) {
                           $rootScope.isLogin = true;
                           var user = data.datas.user;
                           $rootScope.clearAllInfo();
                           $rootScope.addInfo("success", "Login success. Redirecting to home page...");

                           var json = userService.GetUserInfo({
                               username: user.Username
                           }, function() {
                               var userInfo = json.data;
                               $session.set(userInfo);
                               //$scope.$$prevSibling.refetchModuleGroups();
                               $timeout(function() {
                                   $rootScope.showNavigation = true;
                                   $location.path(userInfo.Role.DefaultRoute);
                                   $rootScope.clearAllInfo();
                               }, 1000);
                           });
                       } else {
                           $rootScope.clearAllInfo();
                           if (data.validations != null) {
                               angular.forEach(data.validations, function(v, i) {
                                   $rootScope.addInfo("error", v.message);
                               });
                           }
                       }
                   });
               };
           }]);