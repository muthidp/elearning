﻿angular.module('app')
    .controller('NavigationBarController', ['$rootScope', '$scope', '$state', '$timeout', '$location', '$session', '$q', 'NavigationBarService', 'AuthenticationService', 'ModuleGroupService',
        function ($rootScope, $scope, $state, $timeout, $location, $session, $q, navigationBarService, authService, moduleGroupService) {

            $rootScope.waitInitialized(function () {
                $scope.showNavigationBar = navigationBarService.getIsShow();
                $scope.moduleGroups = [];
                var jsonMg = moduleGroupService.GetAllModuleGroups();

                $scope.refetchModuleGroups = function () {
                    moduleGroupService.GetAllModuleGroups();
                };

                $q.all([jsonMg.$promise])['finally'](function () {
                    $scope.moduleGroups = jsonMg.data;
                    console.log("MODULE GROUPA ", $scope.moduleGroups);
                });

                var user = $session.get();
                $scope.user = user;

                $scope.navigatePage = function (route) {
                    if (route != null && route != "")
                        $location.path(route);
                };

                $scope.hasAcl = function (aclKey) {
                    var result = false;
                    if (user != null && user.Acls != null) {
                        angular.forEach(user.Acls, function (acl, i) {
                            if (aclKey == acl) result = true;
                        });
                    }
                    return result;
                };

                $scope.onLogin = function () {
                    navigationBarService.hide();
                    $scope.showNavigationBar = navigationBarService.getIsShow();
                    $location.path('/login');
                };

                $scope.onLogout = function () {
                    var json = authService.Logout(function () {
                        var data = json.data;
                        if (data) {
                            $rootScope.isLogin = false;
                            $session.set(null);
                            $scope.moduleGroups = [];
                            $location.path('/home');
                            $rootScope.clearAllInfo();
                            $rootScope.addInfo("success", "Successfully logout.");
                            $timeout(function () {
                                $rootScope.clearAllInfo();
                            }, 3000);
                        }
                    });
                };
            });
        }]);