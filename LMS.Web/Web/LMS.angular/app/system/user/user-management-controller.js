﻿angular.module('app')
    .controller('UserManagementController',
    ['$rootScope', '$scope', '$http', '$resource', '$modal', 'UserService', 'RoleService', '$timeout', '$session',
        function ($rootScope, $scope, $http, $resource, $modal, UserService, RoleService, $timeout, $session) {
            $rootScope.waitInitialized(function () {
                var userSession = $session.get();
                //console.log("USER SESSION ", userSession);

                $scope.roles = [];

                var openModal = function (data) {
                    //console.log("OPEN MODAL ", data);
                    $modal.open({
                        templateUrl: APP_URL + "system/user/user-form.html",
                        controller: 'UserFormController',
                        animation: true,
                        resolve: {
                            userData: function () {
                                return data;
                            },
                            roleData: function () {
                                return $scope.roles;
                            }
                        }
                    }).result.then(function (result) {
                        $scope.Fetch();
                    });
                };

                $scope.SearchQuery = {
                    Page: 1,
                    RowPerPage: 10,
                    SortBy: 'Username',
                    IsSortAsc: true,
                    TotalData: 0,
                    MaxPage: 8,
                    Search: {
                        Keyword: '',
                        Fields: ['Username', 'Email', 'RoleLabel', 'InstitutionLabel']
                    }
                };

                $scope.users = [];
                $scope.Fetch = function () {
                    $rootScope.showLoading();
                    var jsonResult = UserService.GetAllForView({
                        institutionId: userSession.Institution.InstitutionId,
                        queryModel: $scope.SearchQuery
                    }, function () {
                        var data = jsonResult.data;
                        $scope.users = data.list;

                        //console.log("Users ::", $scope.users);

                        $scope.SearchQuery.TotalData = data.totalData;
                        $scope.SearchQuery.TotalPage = data.totalPage;
                        $rootScope.hideLoading();
                    });
                };

                var jsonRoleResult = RoleService.GetAll({
                }, function () {
                    $scope.roles = jsonRoleResult.data;
                });


                $scope.onCreateNew = function () {

                    var user = {
                        UserId: null,
                        InstitutionId: userSession.Institution.InstitutionId,
                        DefaultRoute: ''
                    };
                    openModal(user);
                };
                $scope.onRowEdit = function (userData) {
                    openModal(angular.copy(userData));
                };
                $scope.onRowDelete = function (userData) {
                    var sel = confirm("Are you sure you want to delete the user [" + userData.Username + "] ?");
                    if (sel) {
                        var json = UserService.Delete({
                            UserId: userData.UserId
                        }, function () {
                            if (json.data.success) {
                                $scope.Fetch();
                                $rootScope.clearAllInfo();
                                $rootScope.addInfo("success", "User successfully deleted.");
                                $timeout(function () {
                                    $rootScope.clearAllInfo();
                                }, 3000);
                            }
                        });
                    }
                };

                $scope.onSearch = function () {
                    $scope.Fetch();
                };
                $scope.onPageChanged = function (page) {
                    $scope.SearchQuery.Page = page;
                    $scope.Fetch();
                };
                $scope.onSort = function (sortField) {
                    if ($scope.SearchQuery.SortBy == sortField)
                        $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
                    else
                        $scope.SearchQuery.IsSortAsc = true;
                    $scope.SearchQuery.SortBy = sortField;
                    $scope.Fetch();
                };
                $scope.onKeyPress = function ($event) {
                    if ($event.keyCode === 13) {
                        $scope.Fetch();
                    }
                };

                $scope.Fetch();
            });
        }
    ]);
