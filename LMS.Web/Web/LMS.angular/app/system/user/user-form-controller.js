﻿angular.module('app')
    .controller('UserFormController',
    ['$scope', 'UserService', '$http', '$rootScope', '$resource', '$modalInstance', '$timeout', '$session', 'userData',
        'roleData', 'ToastMessageService',
        function ($scope, UserService, $http, $rootScope, $resource, $modalInstance, $timeout, $session, userData,
            roleData, ToastMessageService) {

            var userSession = $session.get();
            $scope.user = userData;
            $scope.roles = roleData;

            $scope.user.Password = "";
            $scope.reset = function () {
                $scope.user = {
                    Username: '',
                    Email: '',
                    Password: '',
                    Confirm_Password: '',
                    RoleId: '',
                    institution_id: userSession.Institution.InstitutionId
                };
            };

            $scope.validations = {
                Username: false,
                Email: false,
                not_email: false,
                Password: false,
                retype: false,
                retype_not_matching: false,
                Role: false
            }


            function isEmpty(str) {
                return (!str || 0 === str.length);
            }

            //validate if email is entered correctly
            function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            $scope.onSave = function () {
                //$rootScope.showLoading();
                var proceed = true;

                if (isEmpty($scope.user.Username)) {
                    $scope.validations.Username = true;
                    $timeout(function () {
                        $scope.validations.Username = false;
                    }, 5000);
                    proceed = false;
                } else {
                    $scope.user.Username = $scope.user.Username.trim();
                }

                if (isEmpty($scope.user.RoleId)) {
                    $scope.validations.Role = true;
                    $timeout(function () {
                        $scope.validations.Role = false;
                    }, 5000);
                    proceed = false;
                }

                if (isEmpty($scope.user.Email)) {
                    $scope.validations.Email = true;
                    $timeout(function () {
                        $scope.validations.Email = false;
                    }, 5000);
                    proceed = false;
                } else {
                    $scope.user.Email = $scope.user.Email.trim();

                    if (!validateEmail($scope.user.Email)) {
                        $scope.validations.not_email = true;
                        $timeout(function () {
                            $scope.validations.not_email = false;
                        }, 5000);
                        proceed = false;
                    }
                }

                if (isEmpty($scope.user.Password)) {
                    $scope.validations.Password = true;
                    $timeout(function () {
                        $scope.validations.Password = false;
                    }, 5000);
                    proceed = false;
                }

                if (!isEmpty($scope.user.Password)) {
                    if (isEmpty($scope.user.Confirm_Password)) {
                        $scope.validations.retype = true;
                        $timeout(function () {
                            $scope.validations.retype = false;
                        }, 5000);
                        proceed = false;
                    }
                }

                if ($scope.user.Password != $scope.user.Confirm_Password) {
                    $scope.validations.retype_not_matching = true;
                    $timeout(function () {
                        $scope.validations.retype_not_matching = false;
                    }, 5000);
                    proceed = false;
                }

                if (proceed) {

                    var c_result = UserService.CheckLimit({
                        InstitutionId: userSession.Institution.InstitutionId
                    }, function () {
                        if (c_result.data.result) {

                            var result = UserService.Save($scope.user, function () {
                                if (result.data.success) {
                                    ToastMessageService.showSaveMessage('User');
                                    $modalInstance.close();
                                } else {
                                    $rootScope.hideLoading();
                                    $scope.reset();
                                    $rootScope.clearAllInfo();
                                    $rootScope.addInfo("error", "Registration is failed. Please contact an adminstrator.");
                                    $modalInstance.close();
                                    $timeout(function () {
                                        $rootScope.clearAllInfo();
                                    }, 8000);
                                }
                            });

                        } else {
                            $rootScope.clearAllInfo();
                            $rootScope.addInfo("error", c_result.data.msg);
                            //$modalInstance.close();
                            $timeout(function () {
                                $rootScope.clearAllInfo();
                            }, 4000);
                        }
                    });





                    
                } else {
                    $rootScope.hideLoading();
                }
            };

            $scope.onCancel = function (_nutrient) {

                //var sel = confirm("Are you sure you want to cancel? Your changes will not be saved.");
                //if (sel) {
                //    $modalInstance.dismiss('cancel');
                //    $rootScope.clearAllInfo();
                //}

                $modalInstance.dismiss('cancel');
                $rootScope.clearAllInfo();
            };
        }
    ]);