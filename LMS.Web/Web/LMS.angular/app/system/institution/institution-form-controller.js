﻿angular.module('app')
    .controller('InstitutionFormController',
    ['$scope', 'InstitutionService', '$http', '$rootScope', '$resource', '$modalInstance', '$timeout', '$session', 'InstitutionData',
        'ToastMessageService',
        function ($scope, InstitutionService, $http, $rootScope, $resource, $modalInstance, $timeout, $session, InstitutionData,
            ToastMessageService) {

            $scope.institution = InstitutionData;

            $scope.reset = function () {
                $scope.institution = {
                    InstitutionCode: '',
                    InstitutionId: null,
                    InstitutionLabel: '',
                };
            };

            $scope.validations = {
                InstitutionLabel: false,
                InstitutionCode: false
            }


            function isEmpty(str) {
                return (!str || 0 === str.length);
            }


            $scope.minus = function () {
                if ($scope.institution.UserLimit > 0) {
                    $scope.institution.UserLimit--;
                }
            }

            $scope.plus = function () {
                $scope.institution.UserLimit++;
            }

            $scope.onSave = function () {
                $rootScope.showLoading();
                var proceed = true;

                if (isEmpty($scope.institution.InstitutionLabel)) {
                    $scope.validations.InstitutionLabel = true;
                    $timeout(function () {
                        $scope.validations.InstitutionLabel = false;
                    }, 5000);
                    proceed = false;
                } else {
                    $scope.institution.InstitutionLabel = $scope.institution.InstitutionLabel.trim();
                }

                if (isEmpty($scope.institution.InstitutionCode)) {
                    $scope.validations.InstitutionCode = true;
                    $timeout(function () {
                        $scope.validations.InstitutionCode = false;
                    }, 5000);
                    proceed = false;
                } else {
                    $scope.institution.InstitutionCode = $scope.institution.InstitutionCode.trim();
                }

                if (isEmpty($scope.institution.UserLimit)) {
                    $scope.institution.UserLimit = 0;
                }

                if (proceed) {
                    var result = InstitutionService.Save($scope.institution, function () {
                        if (result.data.success) {
                            $rootScope.hideLoading();
                            ToastMessageService.showSaveMessage('Institution');
                            $modalInstance.close();
                        } else {
                            $rootScope.hideLoading();
                            $scope.reset();
                            $rootScope.clearAllInfo();
                            $rootScope.addInfo("error", "Saving failed. Please contact an adminstrator.");
                            $modalInstance.close();
                            $timeout(function () {
                                $rootScope.clearAllInfo();
                            }, 4000);
                        }
                    });
                } else {
                    $rootScope.hideLoading();
                }
            };

            $scope.onCancel = function (_nutrient) {
                $modalInstance.dismiss('cancel');
                $rootScope.clearAllInfo();
            };
        }
    ]);