﻿angular.module('app')
    .controller('InstitutionManagementController',
    ['$rootScope', '$scope', '$http', '$resource', '$modal', 'InstitutionService', 'InstitutionService', '$timeout', '$session',
        function ($rootScope, $scope, $http, $resource, $modal, InstitutionService, InstitutionService, $timeout, $session) {
            $rootScope.waitInitialized(function () {
                var userSession = $session.get();
                //console.log("User SESSION ", userSession);

                $scope.institutions = [];

                var openModal = function (data) {
                    console.log("OPEN MODAL ", data);
                    $modal.open({
                        templateUrl: APP_URL + "system/institution/institution-form.html",
                        controller: 'InstitutionFormController',
                        animation: true,
                        resolve: {
                            InstitutionData: function () {
                                return data;
                            }
                        }
                    }).result.then(function (result) {
                        $scope.Fetch();
                    });
                };

                $scope.SearchQuery = {
                    Page: 1,
                    RowPerPage: 10,
                    SortBy: 'InstitutionCode',
                    IsSortAsc: true,
                    TotalData: 0,
                    MaxPage: 8,
                    Search: {
                        Keyword: '',
                        Fields: ['InstitutionCode', 'InstitutionLabel']
                    }
                };

                $scope.institutions = [];
                $scope.Fetch = function () {
                    $rootScope.showLoading();
                    var jsonResult = InstitutionService.GetAllForView({
                        queryModel: $scope.SearchQuery
                    }, function () {
                        var data = jsonResult.data;
                        $scope.institutions = data.list;

                        console.log("institutions ::", $scope.institutions);

                        $scope.SearchQuery.TotalData = data.totalData;
                        $scope.SearchQuery.TotalPage = data.totalPage;
                        $rootScope.hideLoading();
                    });
                };

                $scope.onCreateNew = function () {
                    var Institution = {
                        InstitutionId: null,
                        UserLimit: 100,
                        Subscribed: true
                    };
                    openModal(Institution);
                };
                $scope.onRowEdit = function (InstitutionData) {
                    openModal(angular.copy(InstitutionData));
                };
                $scope.onRowDelete = function (InstitutionData) {
                    var sel = confirm("Are you sure you want to delete the institution [" + InstitutionData.InstitutionCode + "] ?");
                    if (sel) {
                        var json = InstitutionService.Delete({
                            InstitutionId: InstitutionData.InstitutionId
                        }, function () {
                            if (json.data.success) {
                                $scope.Fetch();
                                $rootScope.clearAllInfo();
                                $rootScope.addInfo("success", "Institution successfully deleted.");
                                $timeout(function () {
                                    $rootScope.clearAllInfo();
                                }, 3000);
                            }
                        });
                    }
                };

                $scope.onSearch = function () {
                    $scope.Fetch();
                };
                $scope.onPageChanged = function (page) {
                    $scope.SearchQuery.Page = page;
                    $scope.Fetch();
                };
                $scope.onSort = function (sortField) {
                    if ($scope.SearchQuery.SortBy == sortField)
                        $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
                    else
                        $scope.SearchQuery.IsSortAsc = true;
                    $scope.SearchQuery.SortBy = sortField;
                    $scope.Fetch();
                };
                $scope.onKeyPress = function ($event) {
                    if ($event.keyCode === 13) {
                        $scope.Fetch();
                    }
                };

                $scope.Fetch();
            });
        }
    ]);
