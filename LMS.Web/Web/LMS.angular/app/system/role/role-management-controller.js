﻿angular.module('app')
    .controller('RoleManagementController',
    ['$rootScope', '$scope', '$http', '$resource', '$modal', 'RoleService', 'RoleService', '$timeout', '$session',
        function ($rootScope, $scope, $http, $resource, $modal, RoleService, RoleService, $timeout, $session) {
            $rootScope.waitInitialized(function () {
                var userSession = $session.get();
                //console.log("User SESSION ", userSession);

                $scope.roles = [];

                var openModal = function (data) {
                    console.log("OPEN MODAL ", data);
                    $modal.open({
                        templateUrl: APP_URL + "system/role/role-form.html",
                        controller: 'RoleFormController',
                        animation: true,
                        resolve: {
                            RoleData: function () {
                                return data;
                            }
                        }
                    }).result.then(function (result) {
                        $scope.Fetch();
                    });
                };

                $scope.SearchQuery = {
                    Page: 1,
                    RowPerPage: 10,
                    SortBy: 'RoleCode',
                    IsSortAsc: true,
                    TotalData: 0,
                    MaxPage: 8,
                    Search: {
                        Keyword: '',
                        Fields: ['RoleCode', 'RoleLabel']
                    }
                };

                $scope.roles = [];
                $scope.Fetch = function () {
                    $rootScope.showLoading();
                    var jsonResult = RoleService.GetAllForView({
                        queryModel: $scope.SearchQuery
                    }, function () {
                        var data = jsonResult.data;
                        $scope.roles = data.list;

                        console.log("Roles ::", $scope.roles);

                        $scope.SearchQuery.TotalData = data.totalData;
                        $scope.SearchQuery.TotalPage = data.totalPage;
                        $rootScope.hideLoading();
                    });
                };

                $scope.onCreateNew = function () {
                    var Role = {
                        RoleId: null,
                        DefaultRoute: ''
                    };
                    openModal(Role);
                };
                $scope.onRowEdit = function (RoleData) {
                    openModal(angular.copy(RoleData));
                };
                $scope.onRowDelete = function (RoleData) {
                    var sel = confirm("Are you sure you want to delete the Role [" + RoleData.RoleCode + "] ?");
                    if (sel) {
                        var json = RoleService.Delete({
                            RoleId: RoleData.RoleId
                        }, function () {
                            if (json.data.success) {
                                $scope.Fetch();
                                $rootScope.clearAllInfo();
                                $rootScope.addInfo("success", "Role successfully deleted.");
                                $timeout(function () {
                                    $rootScope.clearAllInfo();
                                }, 3000);
                            }
                        });
                    }
                };

                $scope.onSearch = function () {
                    $scope.Fetch();
                };
                $scope.onPageChanged = function (page) {
                    $scope.SearchQuery.Page = page;
                    $scope.Fetch();
                };
                $scope.onSort = function (sortField) {
                    if ($scope.SearchQuery.SortBy == sortField)
                        $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
                    else
                        $scope.SearchQuery.IsSortAsc = true;
                    $scope.SearchQuery.SortBy = sortField;
                    $scope.Fetch();
                };
                $scope.onKeyPress = function ($event) {
                    if ($event.keyCode === 13) {
                        $scope.Fetch();
                    }
                };

                $scope.Fetch();
            });
        }
    ]);
