﻿angular.module('app')
    .controller('RoleFormController',
    ['$scope', 'RoleService', '$http', '$rootScope', '$resource', '$modalInstance', '$timeout', '$session', 'RoleData',
        'ToastMessageService',
        function ($scope, RoleService, $http, $rootScope, $resource, $modalInstance, $timeout, $session, RoleData,
            ToastMessageService) {

            $scope.role = RoleData;

            $scope.reset = function () {
                $scope.role = {
                    RoleCode: '',
                    RoleId: null,
                    RoleLabel: '',
                    DefaultRoute: ''
                };
            };

            $scope.validations = {
                RoleLabel: false,
                RoleCode: false
            }


            function isEmpty(str) {
                return (!str || 0 === str.length);
            }

            $scope.onSave = function () {
                $rootScope.showLoading();
                var proceed = true;

                if (isEmpty($scope.role.RoleLabel)) {
                    $scope.validations.RoleLabel = true;
                    $timeout(function () {
                        $scope.validations.RoleLabel = false;
                    }, 5000);
                    proceed = false;
                } else {
                    $scope.role.RoleLabel = $scope.role.RoleLabel.trim();
                }

                if (isEmpty($scope.role.RoleCode)) {
                    $scope.validations.RoleCode = true;
                    $timeout(function () {
                        $scope.validations.RoleCode = false;
                    }, 5000);
                    proceed = false;
                } else {
                    $scope.role.RoleCode = $scope.role.RoleCode.trim();
                }

                if (proceed) {
                    var result = RoleService.Save($scope.role, function () {
                        if (result.data.success) {
                            $rootScope.hideLoading();
                            ToastMessageService.showSaveMessage('Role');
                            $modalInstance.close();
                        } else {
                            $rootScope.hideLoading();
                            $scope.reset();
                            $rootScope.clearAllInfo();
                            $rootScope.addInfo("error", "Saving failed. Please contact an adminstrator.");
                            $modalInstance.close();
                            $timeout(function () {
                                $rootScope.clearAllInfo();
                            }, 4000);
                        }
                    });
                } else {
                    $rootScope.hideLoading();
                }
            };

            $scope.onCancel = function (_nutrient) {
                $modalInstance.dismiss('cancel');
                $rootScope.clearAllInfo();
            };
        }
    ]);