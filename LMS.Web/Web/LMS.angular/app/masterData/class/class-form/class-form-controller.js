﻿angular.module('app')
    .controller('ClassFormController',
    ['$scope', 'CourseService', '$http', '$rootScope', '$resource', '$modalInstance', '$timeout', '$session', 'classData', 'ClassService', 'UserService', 'ToastMessageService',
        function ($scope, courseService, $http, $rootScope, $resource, $modalInstance, $timeout, $session, classData, classService, userService, toastMessageService) {

            var userSession = $session.get();

            $scope.class = classData;
            if ($scope.class == null) {
                $scope.class = {
                    ClassId: null,
                    ClassCode: null,
                    ClassLabel: null,
                    CourseId: null,
                    InstructorId: null,
                    CoInstructorId: null
                };
            }

            $scope.courses = [];
            var jsonCourse = courseService.GetAll({
                institutionId: userSession.Institution.InstitutionId
            }, function() {
                $scope.courses = jsonCourse.data;
            });

            $scope.instructors = [];
            var jsonIns = userService.GetUserInstructors({
                institutionId: userSession.Institution.InstitutionId
            }, function () {
                $scope.instructors = jsonIns.data;
            });

            $scope.validations = {};
            $scope.onSave = function () {
                $rootScope.showLoading();
                var json = classService.Save({
                    classModel: $scope.class
                }, function () {
                    if (json.data.success) {
                        $modalInstance.close();
                        toastMessageService.showSaveMessage('Class');
                    } else {
                        if (json.data.validations != null) {
                            $scope.validations = json.data.validations;
                        }
                    }
                    $rootScope.hideLoading();
                });
            };

            $scope.onCancel = function () {
                $modalInstance.close();
            };
        }
    ]);