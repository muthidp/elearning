﻿angular.module('app')
    .controller('ClassListController',
    ['$rootScope', '$scope', '$http', '$resource', '$modal', 'ClassService', '$timeout', '$session', 'ToastMessageService',
    function ($rootScope, $scope, $http, $resource, $modal, classService, $timeout, $session, toastMessageService) {
        $rootScope.waitInitialized(function () {
            var userSession = $session.get();

            var openModal = function (data) {
                $modal.open({
                    templateUrl: APP_URL + "masterData/class/class-form/class-form.html",
                    controller: 'ClassFormController',
                    animation: true,
                    resolve: {
                        classData: function () {
                            return data;
                        }
                    }
                }).result.then(function (result) {
                    $scope.Fetch();
                });
            };

            var openStudentClassModal = function (data) {
                $modal.open({
                    templateUrl: APP_URL + "masterData/class/student-class-form/student-class-form.html",
                    controller: 'StudentClassFormController',
                    animation: true,
                    resolve: {
                        classData: function () {
                            return data;
                        }
                    }
                }).result.then(function (result) {
                    $scope.Fetch();
                });
            };

            $scope.SearchQuery = {
                Page: 1,
                RowPerPage: 10,
                SortBy: 'ClassCode',
                IsSortAsc: true,
                TotalData: 0,
                MaxPage: 8,
                Search: {
                    Keyword: '',
                    Fields: ['ClassCode', 'ClassLabel']
                }
            };

            $scope.classes = [];
            $scope.Fetch = function () {
                $rootScope.showLoading();
                var jsonResult = classService.GetAllForView({
                    queryModel: $scope.SearchQuery
                }, function () {
                    var data = jsonResult.data;
                    $scope.classes = data.list;
                    $scope.SearchQuery.TotalData = data.totalData;
                    $scope.SearchQuery.TotalPage = data.totalPage;
                    $rootScope.hideLoading();
                });
            };

            $scope.onCreateNew = function () {
                openModal(null);
            };
            $scope.onRowEdit = function (classData) {
                openModal(angular.copy(classData));
            };
            $scope.onRowAddStudent = function (classData) {
                openStudentClassModal(angular.copy(classData));
            };
            $scope.onRowDelete = function (classData) {
                var sel = confirm("Are you sure you want to delete the course [" + classData.ClassLabel + "] ?");
                if (sel) {
                    var json = classService.Delete({
                        classId: classData.ClassId
                    }, function () {
                        if (json.data.success) {
                            toastMessageService.showDeleteMessage("Class");
                            $scope.Fetch();
                        }
                    });
                }
            };

            $scope.onSearch = function () {
                $scope.Fetch();
            };
            $scope.onPageChanged = function (page) {
                $scope.SearchQuery.Page = page;
                $scope.Fetch();
            };
            $scope.onSort = function (sortField) {
                if ($scope.SearchQuery.SortBy == sortField)
                    $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
                else
                    $scope.SearchQuery.IsSortAsc = true;
                $scope.SearchQuery.SortBy = sortField;
                $scope.Fetch();
            };
            $scope.onKeyPress = function ($event) {
                if ($event.keyCode === 13) {
                    $scope.Fetch();
                }
            };

            $scope.Fetch();
        });
    }
    ]);