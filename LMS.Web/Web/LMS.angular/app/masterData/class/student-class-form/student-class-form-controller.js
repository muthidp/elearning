﻿angular.module('app')
    .controller('StudentClassFormController',
    ['$scope', 'CourseService', '$http', '$rootScope', '$resource', '$modalInstance', '$timeout', '$session', 'classData', 'ClassService', 'UserService', 'ToastMessageService',
        function ($scope, courseService, $http, $rootScope, $resource, $modalInstance, $timeout, $session, classData, classService, userService, toastMessageService) {

            var userSession = $session.get();

            $scope.class = classData;
            if ($scope.class == null) {
                $scope.class = {
                    ClassId: null,
                    ClassCode: null,
                    ClassLabel: null,
                    CourseId: null,
                    InstructorId: null,
                    CoInstructorId: null
                };
            }

            $scope.classStudents = [];
            var fetchClassStudents = function() {
                var jsonCs = classService.GetStudentsUnderClass({
                    classId: $scope.class.ClassId
                }, function() {
                    $scope.classStudents = jsonCs.data;
                });
            };
            fetchClassStudents();

            $scope.courses = [];
            var jsonCourse = courseService.GetAll({
                institutionId: userSession.Institution.InstitutionId
            }, function () {
                $scope.courses = jsonCourse.data;
            });

            $scope.students = [];
            var jsonStu = userService.GetUserStudents({
                institutionId: userSession.Institution.InstitutionId
            }, function () {
                $scope.students = jsonStu.data;
            });

            $scope.selected = {
                student: null
            };

            $scope.onAddStudent = function(student) {
                var json = classService.AddStudentToClass({
                    userId: student.UserId,
                    classId: $scope.class.ClassId
                }, function() {
                    if (json.data.success) {
                        toastMessageService.showInfo("success", "Student added succesfully.");
                        fetchClassStudents();
                        $scope.selected.student = null;
                    }
                });
            };

            $scope.onDeleteStudent = function (model) {
                var sel = confirm("Are you sure you want to delete this student from class?");
                if (sel) {
                    var json = classService.DeleteStudentFromClass({
                        studentClassId: model.StudentClassId
                    }, function() {
                        if (json.data.success) {
                            toastMessageService.showDeleteMessage("Student");
                            fetchClassStudents();
                        }
                    });
                }
            }

            $scope.validations = {};

            $scope.onClose = function () {
                $modalInstance.close();
            };
        }
    ]);