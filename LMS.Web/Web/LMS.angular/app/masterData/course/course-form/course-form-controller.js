﻿angular.module('app')
    .controller('CourseFormController',
    ['$scope', 'CourseService', '$http', '$rootScope', '$resource', '$modalInstance', '$timeout', '$session', 'courseData',
        function ($scope, courseService, $http, $rootScope, $resource, $modalInstance, $timeout, $session, courseData) {

            var userSession = $session.get();

            $scope.course = courseData;
            if ($scope.course == null) {
                $scope.course = {
                    CourseId: null,
                    CourseCode: null,
                    CourseLabel: null,
                    Description: null,
                    YearIntake: null,
                    InstitutionId: userSession.Institution.InstitutionId,
                    PictureUrl: null
                };
            }

            $scope.fileResult = {};

            $scope.validations = {};
            $scope.onSave = function () {
                $rootScope.showLoading();
                var json = courseService.Save({
                    course: $scope.course
                }, function () {
                    if (json.data.success) {
                        $modalInstance.close();
                        $rootScope.clearAllInfo();
                        $rootScope.addInfo("success", "Course saved successfully.");
                        $timeout(function() {
                            $rootScope.clearAllInfo();
                        }, 3000);
                    } else {
                        if (json.data.validations != null) {
                            $scope.validations = json.data.validations;
                        }
                    }
                    $rootScope.hideLoading();
                });
            };

            $scope.onPick = function(fileResult) {
                console.log("on pick broooh");
                $scope.course.PictureUrl = fileResult.RelativePath;
            };

            $scope.onCancel = function () {
                $modalInstance.close();
            };
        }
    ]);