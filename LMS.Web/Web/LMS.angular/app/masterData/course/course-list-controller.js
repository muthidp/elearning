﻿angular.module('app')
    .controller('CourseListController',
    ['$rootScope', '$scope', '$http', '$resource', '$modal', 'CourseService', '$timeout', '$session',
        function ($rootScope, $scope, $http, $resource, $modal, courseService, $timeout, $session) {
            $rootScope.waitInitialized(function () {
                var userSession = $session.get();
                console.log("USER SESSION ", userSession);

                var openModal = function (data) {
                    $modal.open({
                        templateUrl: APP_URL + "masterData/course/course-form/course-form.html",
                        controller: 'CourseFormController',
                        animation: true,
                        resolve: {
                            courseData: function () {
                                return data;
                            }
                        }
                    }).result.then(function (result) {
                        $scope.Fetch();
                    });
                };

                $scope.SearchQuery = {
                    Page: 1,
                    RowPerPage: 10,
                    SortBy: 'CourseCode',
                    IsSortAsc: true,
                    TotalData: 0,
                    MaxPage: 8,
                    Search: {
                        Keyword: '',
                        Fields: ['CourseCode', 'CourseLabel', 'Description']
                    }
                };

                $scope.courses = [];
                $scope.Fetch = function () {
                    $rootScope.showLoading();
                    var jsonResult = courseService.GetAllForView({
                        institutionId: userSession.Institution.InstitutionId,
                        queryModel: $scope.SearchQuery
                    }, function () {
                        var data = jsonResult.data;
                        $scope.courses = data.list;
                        $scope.SearchQuery.TotalData = data.totalData;
                        $scope.SearchQuery.TotalPage = data.totalPage;
                        $rootScope.hideLoading();
                    });
                };

                $scope.onCreateNew = function () {
                    openModal(null);
                };
                $scope.onRowEdit = function (courseData) {
                    openModal(angular.copy(courseData));
                };
                $scope.onRowDelete = function (courseData) {
                    var sel = confirm("Are you sure you want to delete the course [" + courseData.CourseLabel + "] ?");
                    if (sel) {
                        var json = courseService.Delete({
                            courseId: courseData.CourseId
                        }, function () {
                            if (json.data.success) {
                                $rootScope.clearAllInfo();
                                $rootScope.addInfo("success", "Course successfully deleted.");
                                $timeout(function () {
                                    $rootScope.clearAllInfo();
                                }, 3000);
                            }
                        });
                    }
                };

                $scope.onSearch = function () {
                    $scope.Fetch();
                };
                $scope.onPageChanged = function (page) {
                    $scope.SearchQuery.Page = page;
                    $scope.Fetch();
                };
                $scope.onSort = function (sortField) {
                    if ($scope.SearchQuery.SortBy == sortField)
                        $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
                    else
                        $scope.SearchQuery.IsSortAsc = true;
                    $scope.SearchQuery.SortBy = sortField;
                    $scope.Fetch();
                };
                $scope.onKeyPress = function ($event) {
                    if ($event.keyCode === 13) {
                        $scope.Fetch();
                    }
                };

                $scope.Fetch();
            });
        }
    ]);
