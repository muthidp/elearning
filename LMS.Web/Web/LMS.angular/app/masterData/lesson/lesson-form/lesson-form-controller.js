﻿angular.module('app')
    .controller('LessonFormController',
    ['$scope', 'ClassService', '$http', '$rootScope', '$resource', '$modalInstance', '$timeout', '$session', 'lessonData', 'LessonService',
        function ($scope, classService, $http, $rootScope, $resource, $modalInstance, $timeout, $session, lessonData, lessonService) {

            var userSession = $session.get();

            $scope.classes = [];
            var jsonCourse = classService.GetAll({
                institutionId: userSession.Institution.InstitutionId
            }, function() {
                $scope.classes = jsonCourse.data;
            });

            $scope.lesson = lessonData;
            if ($scope.lesson == null) {
                $scope.lesson = {
                    LessonId: null,
                    LessonCode: null,
                    LessonLabel: null,
                    ClassId: null
                };
            }

            $scope.validations = {};
            $scope.onSave = function () {
                $rootScope.showLoading();
                var json = lessonService.Save({
                    lessonModel: $scope.lesson
                }, function () {
                    if (json.data.success) {
                        $modalInstance.close();
                        $rootScope.clearAllInfo();
                        $rootScope.addInfo("success", "Lesson saved successfully.");
                        $timeout(function() {
                            $rootScope.clearAllInfo();
                        }, 3000);
                    } else {
                        if (json.data.validations != null) {
                            $scope.validations = json.data.validations;
                        }
                    }
                    $rootScope.hideLoading();
                });
            };

            $scope.onCancel = function () {
                $modalInstance.close();
            };
        }
    ]);