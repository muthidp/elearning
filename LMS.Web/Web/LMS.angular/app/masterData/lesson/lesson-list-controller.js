﻿angular.module('app')
    .controller('LessonListController',
    ['$rootScope', '$scope', '$http', '$resource', '$modal', 'LessonService', '$timeout', '$session',
        function ($rootScope, $scope, $http, $resource, $modal, lessonService, $timeout, $session) {
            $rootScope.waitInitialized(function () {
                var userSession = $session.get();

                var openModal = function (data) {
                    $modal.open({
                        templateUrl: APP_URL + "masterData/lesson/lesson-form/lesson-form.html",
                        controller: 'LessonFormController',
                        animation: true,
                        resolve: {
                            lessonData: function () {
                                return data;
                            }
                        }
                    }).result.then(function (result) {
                        $scope.Fetch();
                    });
                };

                $scope.SearchQuery = {
                    Page: 1,
                    RowPerPage: 10,
                    SortBy: 'LessonCode',
                    IsSortAsc: true,
                    TotalData: 0,
                    MaxPage: 8,
                    Search: {
                        Keyword: '',
                        Fields: ['LessonCode', 'LessonLabel']
                    }
                };

                $scope.lessons = [];
                $scope.Fetch = function () {
                    $rootScope.showLoading();
                    var jsonResult = lessonService.GetAllForView({
                        queryModel: $scope.SearchQuery
                    }, function () {
                        var data = jsonResult.data;
                        $scope.lessons = data.list;
                        $scope.SearchQuery.TotalData = data.totalData;
                        $scope.SearchQuery.TotalPage = data.totalPage;
                        $rootScope.hideLoading();
                    });
                };

                $scope.onCreateNew = function () {
                    openModal(null);
                };
                $scope.onRowEdit = function (lessonData) {
                    openModal(angular.copy(lessonData));
                };
                $scope.onRowDelete = function (lessonData) {
                    var sel = confirm("Are you sure you want to delete the lesson [" + lessonData.LessonLabel + "] ?");
                    if (sel) {
                        var json = lessonService.Delete({
                            lessonId: lessonData.LessonId
                        }, function () {
                            if (json.data.success) {
                                $rootScope.clearAllInfo();
                                $rootScope.addInfo("success", "Lesson successfully deleted.");
                                $timeout(function () {
                                    $rootScope.clearAllInfo();
                                }, 3000);
                            }
                        });
                    }
                };

                $scope.onSearch = function () {
                    $scope.Fetch();
                };
                $scope.onPageChanged = function (page) {
                    $scope.SearchQuery.Page = page;
                    $scope.Fetch();
                };
                $scope.onSort = function (sortField) {
                    if ($scope.SearchQuery.SortBy == sortField)
                        $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
                    else
                        $scope.SearchQuery.IsSortAsc = true;
                    $scope.SearchQuery.SortBy = sortField;
                    $scope.Fetch();
                };
                $scope.onKeyPress = function ($event) {
                    if ($event.keyCode === 13) {
                        $scope.Fetch();
                    }
                };

                $scope.Fetch();
            });
        }
    ]);