﻿var APP_URL = '/Web/LMS.angular/app/';
var htmlClass = {
    website: 'transition-navbar-scroll top-navbar-xlarge bottom-footer',
    websitePricing: 'top-navbar-xlarge bottom-footer app-desktop',
    websiteSurvey: 'top-navbar-xlarge bottom-footer app-desktop app-mobile',
    websiteLogin: 'hide-sidebar ls-bottom-footer',
    websiteTakeQuiz: 'transition-navbar-scroll top-navbar-xlarge bottom-footer app-desktop app-mobile',
    appl3: 'st-layout ls-top-navbar-large ls-bottom-footer show-sidebar sidebar-l3',
    appl1r3: 'st-layout ls-top-navbar-large ls-bottom-footer show-sidebar sidebar-l1 sidebar-r3'
};

angular.module('app').config(
['$urlRouterProvider', '$routeProvider',
    function ($urlRouterProvider, $routeProvider) {

        $routeProvider.when("/student/dashboard", { templateUrl: APP_URL + 'student/student-dashboard.html', controller: 'studentDashboardController' });
        $routeProvider.when("/instructor/dashboard", { templateUrl: APP_URL + 'instructor/instructor-dashboard.html', controller: 'instructorDashboardController' });
        $routeProvider.when("/home", { templateUrl: APP_URL+ "home/home.html", controller: "homeController" });
        $routeProvider.when("/login", { templateUrl: APP_URL + "auth/login.html", controller: "logInController" });

        $routeProvider.when("/masterData/course", { templateUrl: APP_URL + "masterData/course/course-list.html", controller: "CourseListController" });
        $routeProvider.when("/masterData/class", { templateUrl: APP_URL + "masterData/class/class-list.html", controller: "ClassListController" });
        $routeProvider.when("/masterData/lesson", { templateUrl: APP_URL + "masterData/lesson/lesson-list.html", controller: "LessonListController" });

        $routeProvider.when("/fileUpload", { templateUrl: APP_URL + "fileUpload/test.html", controller: "FileUploadTestController" });
        $routeProvider.when("/system/user", { templateUrl: APP_URL + "system/user/user-index.html", controller: "UserManagementController" });
        $routeProvider.when("/system/role", { templateUrl: APP_URL + "system/role/role-index.html", controller: "RoleManagementController" });
        $routeProvider.when("/system/institution", { templateUrl: APP_URL + "system/institution/institution-index.html", controller: "InstitutionManagementController" });

        $routeProvider.when('/sign-up', {
            templateUrl: 'website/sign-up.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.websiteLogin;
                $scope.app.settings.bodyClass = 'login';
                $scope.namesss = "whattt ::";
                            
            }]
        });

        $routeProvider.when('/tutors', {
            url: '/tutors',
            templateUrl: 'website/tutors.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/pricing', {
            templateUrl: 'website/pricing.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.websitePricing;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/survey', {
            templateUrl: 'website/survey.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.websiteSurvey;
                $scope.app.settings.bodyClass = 'survey';
            }]
        });

        $routeProvider.when('/contact', {
            templateUrl: 'website/contact.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/forum/home', {
            templateUrl: 'website/forum-home.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/forum/category', {
            templateUrl: 'website/forum-category.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/forum/thread', {
            templateUrl: 'website/forum-thread.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/blog/listing', {
            templateUrl: 'website/blog-listing.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/blog/post', {
            templateUrl: 'website/blog-post.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/courses/grid', {
            templateUrl: 'website/courses-grid.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/courses/list', {
            templateUrl: 'website/courses-list.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/courses/single', {
            templateUrl: 'website/course.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/student/courses', {
            templateUrl: 'website/student-courses.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/student/take-course', {
            templateUrl: 'website/student-take-course.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/student/course-forums', {
            templateUrl: 'website/student-course-forums.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/student/course-forum-thread', {
            templateUrl: 'website/student-course-forum-thread.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/student/take-quiz', {
            templateUrl: 'website/student-take-quiz.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.websiteTakeQuiz;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/student/messages', {
            templateUrl: 'website/student-messages.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/student/private-profile', {
            templateUrl: 'website/student-private-profile.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/student/billing', {
            templateUrl: 'website/student-billing.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });
        
        $routeProvider.when('/instructor/courses', {
            templateUrl: 'website/instructor-courses.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/instructor/edit-course', {
            templateUrl: 'website/instructor-edit-course.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/instructor/edit-course-meta', {
            templateUrl: 'website/instructor-edit-course-meta.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/instructor/edit-course-lessons', {
            templateUrl: 'website/instructor-edit-course-lessons.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/instructor/earnings', {
            templateUrl: 'website/instructor-earnings.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/instructor/statement', {
            templateUrl: 'website/instructor-statement.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/instructor/messages', {
            templateUrl: 'website/instructor-messages.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/instructor/private-profile', {
            templateUrl: 'website/instructor-private-profile.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.when('/instructor/billing', {
            templateUrl: 'website/instructor-billing.html',
            controller: ['$scope', function($scope){
                $scope.app.settings.htmlClass = htmlClass.website;
                $scope.app.settings.bodyClass = '';
            }]
        });

        $routeProvider.otherwise({ redirectTo: "/home" });
    }
]
);