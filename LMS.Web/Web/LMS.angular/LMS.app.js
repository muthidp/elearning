﻿var DIRECTIVE_PATH = '/Web/LMS.angular/directives/';

angular.module('app')
       .run(['$rootScope', '$state', '$timeout', '$q', 'NavigationBarService', 'InstitutionService', '$location', '$session', 'AuthenticationService', 'UserService',
           '$window',
           function ($rootScope, $state, $timeout, $q, navigationBarService, institutionService, $location, $session, authenticationService, userService,
               $window) {

               var applicationInitialized = false;
               navigationBarService.show();

               $rootScope.isLogin = false;
               $rootScope.showNavigation = true;
               $rootScope.infoActivate = false;
               $rootScope.infoQueue = [];
               $rootScope.clearInfo = function (index) {
                   $rootScope.infoQueue.splice(index, 1);
                   if ($rootScope.infoQueue.length <= 0) {
                       $rootScope.infoActivate = false;
                   }
               };
               $rootScope.clearAllInfo = function (index) {
                   $rootScope.infoQueue = [];
                   $rootScope.infoActivate = false;
               };
               $rootScope.addInfo = function (type, message) {
                   $rootScope.infoQueue.push({
                       type: type,
                       message: message
                   });
                   $rootScope.infoActivate = true;
               };
               $rootScope.setInfo = function (type, message) {
                   $rootScope.clearAllInfo();
                   $rootScope.infoQueue.push({
                       type: type,
                       message: message
                   });
                   $rootScope.infoActivate = true;
               };

               $rootScope.isLoading = false;
               $rootScope.showLoading = function () {
                   $rootScope.isLoading = true;
               };
               $rootScope.hideLoading = function () {
                   $rootScope.isLoading = false;
               };

               $rootScope.showLoading();
               var getUserInfo = function () {
                   var json = userService.GetCurrentUser(function () {
                       var data = json.data;
                       $session.set(data);
                       if (data != null) {
                           if ($location.path() != null && $location.path() != '') {
                               $location.path($location.path());
                           } else {
                               $location.path(data.DefaultRoute);
                           }
                       }
                       applicationInitialized = true;
                       $rootScope.hideLoading();
                   });
               };
               var jsonUser = authenticationService.IsLogin();

               $q.all([jsonUser.$promise])['finally'](function () {
                   var isLogin = jsonUser.data;
                   $rootScope.isLogin = isLogin;
                   $session.setIsLogin(isLogin);
                   if (!isLogin) {
                       $rootScope.hideLoading();
                       navigationBarService.hide();
                       applicationInitialized = true;
                       $location.path('home');
                   } else {
                       getUserInfo();
                   }
               });

               $rootScope.waitInitialized = function (context) {
                   if (applicationInitialized) {
                       context();
                   } else {
                       $timeout(function () {
                           $rootScope.waitInitialized(context);
                       }, 100);
                   }
               };

           }]);
