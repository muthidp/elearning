﻿angular.module('app')

.service('NavigationBarService', [function () {
    var callback = function () {
    };
    var isShow = true;
    return {
        show: function () {
            isShow = true;
            callback();
        },
        registerCallback: function (cb) {
            callback = cb;
        },
        hide: function () {
            isShow = false;
            callback();
        },
        getIsShow: function () {
            return isShow;
        }
    };
}])

.service('$session', [function () {
    var session = null;
    var login = false;
    return {
        get: function () {
            return session;
        },
        set: function (data) {
            session = data;
        },
        isLogin: function () {
            return login;
        },
        setIsLogin: function (data) {
            login = data;
        }
    };

}])

.factory('ToastMessageService', ['$rootScope', '$timeout',
    function ($rootScope, $timeout) {
        var a = $rootScope;

    return {
        showInfo: function (type, message) {
            $rootScope.clearAllInfo();
            $rootScope.addInfo(type, message);
            $timeout(function () {
                $rootScope.clearAllInfo();
            }, 3000);
        },
        showSaveMessage: function (name) {
            $rootScope.clearAllInfo();
            $rootScope.addInfo("success", name + " succesfully saved.");
            $timeout(function () {
                $rootScope.clearAllInfo();
            }, 3000);
        },
        showDeleteMessage: function (name) {
            $rootScope.clearAllInfo();
            $rootScope.addInfo("success", name + " successfully deleted.");
            $timeout(function () {
                $rootScope.clearAllInfo();
            }, 3000);
        }
    };

}])


;