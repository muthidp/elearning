﻿angular.module('FACTORY', ['ngResource'])
    .factory('jQuery', [
        function () {
            return $;
        }
    ]);

angular.module('app').factory('InstitutionService', [
        '$resource', function ($resource) {
            return $resource('/Institution/:verb', {}, {
                GetAllInstitution: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllInstitution'
                    }
                },
                GetInstitutionByCode: {
                    method: 'POST',
                    params: {
                        verb: 'GetInstitutionByCode'
                    }
                },
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
])
    .factory('UserService', [
        '$resource', function ($resource) {
            return $resource('/User/:verb', {}, {
                GetCurrentUser: {
                    method: 'POST',
                    params: {
                        verb: 'GetCurrentUser'
                    }
                },
                GetUserInfo: {
                    method: 'POST',
                    params: {
                        verb: 'GetUserInfo'
                    }
                },
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetUserInstructors: {
                    method: 'POST',
                    params: {
                        verb: 'GetUserInstructors'
                    }
                },
                GetUserStudents: {
                    method: 'POST',
                    params: {
                        verb: 'GetUserStudents'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                },
                CheckLimit: {
                    method: 'POST',
                    params: {
                        verb: 'CheckLimit'
                    }
                }
            });
        }
    ])
    .factory('RoleService', [
        '$resource', function ($resource) {
            return $resource('/Role/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])
    .factory('AuthenticationService', [
        '$resource', function ($resource) {
            return $resource('/Auth/:verb', {}, {
                Login: {
                    method: 'POST',
                    params: {
                        verb: 'Login'

                    }
                },
                IsLogin: {
                    method: 'POST',
                    params: {
                        verb: 'IsLogin'
                    }
                },
                Logout: {
                    method: 'POST',
                    params: {
                        verb: 'Logout'
                    }
                }
            });
        }
    ])
    .factory('ModuleGroupService', [
        '$resource', function ($resource) {
            return $resource('/ModuleGroup/:verb', {}, {
                GetAllModuleGroups: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllModuleGroups'
                    }
                }
            });
        }
    ])

    .factory('CourseService', [
        '$resource', function ($resource) {
            return $resource('/Course/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])

    .factory('ClassService', [
        '$resource', function ($resource) {
            return $resource('/Class/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                },
                GetStudentsUnderClass: {
                    method: 'POST',
                    params: {
                        verb: 'GetStudentsUnderClass'
                    }
                },
                AddStudentToClass: {
                    method: 'POST',
                    params: {
                        verb: 'AddStudentToClass'
                    }
                },
                DeleteStudentFromClass: {
                    method: 'POST',
                    params: {
                        verb: 'DeleteStudentFromClass'
                    }
                }
            });
        }
    ])
    .factory('LessonService', [
        '$resource', function ($resource) {
            return $resource('/Lesson/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])
;

angular.module('app')
.service('Session', ['$window', '$rootScope', function ($window, $rootScope) {

    var userInfo = null;

    if ($window.sessionStorage["UserInfo"] != null) {
        userInfo = JSON.parse($window.sessionStorage["UserInfo"]);
    }

    var session = {
        initializeSession: function () {
            if (userInfo != null)
                $rootScope.User = userInfo;
        },
        setShowNavigation: function (showNavigation) {
            $rootScope.show_navigation = showNavigation;
        },
        setInstiution: function (institution) {
            $rootScope.Institution = institution;
        }
    };

    return session;

}]);