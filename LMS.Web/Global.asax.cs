﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Autofac;
using Autofac.Integration.Mvc;
using log4net;
using LMS.Web.App_Start;
using LMS.Web.ModelBindingFix;
using LMS.Web.Utility.Elmah;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace LMS.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MvcApplication));

        void Application_Error(Object sender, EventArgs e)
        {
            var ex = Server.GetLastError().GetBaseException();

            log.Error("Application Error", ex);
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var builder = new ContainerBuilder();
            AutofacConfig.Register(builder);
            ControllerBuilder.Current.SetControllerFactory(new ErrorHandlingControllerFactory());
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            var jsonValueProviderFactory = ValueProviderFactories.Factories.OfType<JsonValueProviderFactory>().FirstOrDefault();
            ValueProviderFactories.Factories.Remove(jsonValueProviderFactory);
            ValueProviderFactories.Factories.Add(new FixedJsonValueProviderFactory());
            log4net.Config.XmlConfigurator.Configure();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            HttpContext.Current.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
            //FormsAuthentication.SignOut();
        }
    }
}