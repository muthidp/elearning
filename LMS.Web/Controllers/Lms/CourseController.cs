﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Lms;
using LMS.Service.Lms.Course;

namespace LMS.Web.Controllers.Lms
{
    public class CourseController : BaseController
    {
        private readonly ICourseService _courseService;

        public CourseController(
            ICourseService courseService)
        {
            _courseService = courseService;
        }

        public JsonResult GetAllForView(long? institutionId, BaseQueryModel queryModel)
        {
            var courses = _courseService.GetAllWithBaseQuery(institutionId, ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", courses},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var courses = _courseService.GetAll(institutionId);
            return JsonWithContext(courses, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(CourseModel course)
        {
            return JsonWithContext(_courseService.Save(course), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long courseId)
        {
            return JsonWithContext(_courseService.Delete(courseId), JsonRequestBehavior.DenyGet);
        }
    }
}