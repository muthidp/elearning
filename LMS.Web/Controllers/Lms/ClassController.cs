﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Lms;
using LMS.Service.Lms.Class;
using LMS.Service.Lms.Course;

namespace LMS.Web.Controllers.Lms
{
    public class ClassController : BaseController
    {
        private readonly IClassService _classService;

        public ClassController(
            IClassService classService)
        {
            _classService = classService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var classes = _classService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", classes},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll()
        {
            var courses = _classService.GetAll();
            return JsonWithContext(courses, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(ClassModel classModel)
        {
            return JsonWithContext(_classService.Save(classModel), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long classId)
        {
            return JsonWithContext(_classService.Delete(classId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetStudentsUnderClass(long? classId)
        {
            return JsonWithContext(_classService.GetStudentsUnderClass(classId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult AddStudentToClass(long? userId, long? classId)
        {
            return JsonWithContext(_classService.AddStudentToClass(userId, classId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult DeleteStudentFromClass(long? studentClassId)
        {
            return JsonWithContext(_classService.DeleteStudentFromClass(studentClassId), JsonRequestBehavior.DenyGet);
        }
    }
}