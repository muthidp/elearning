﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Lms;
using LMS.Service.Lms.Lesson;

namespace LMS.Web.Controllers.Lms
{
    public class LessonController : BaseController
    {
        private readonly ILessonService _lessonService;

        public LessonController(
            ILessonService lessonService)
        {
            _lessonService = lessonService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var classes = _lessonService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", classes},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll()
        {
            var courses = _lessonService.GetAll();
            return JsonWithContext(courses, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(LessonModel lessonModel)
        {
            return JsonWithContext(_lessonService.Save(lessonModel), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long lessonId)
        {
            return JsonWithContext(_lessonService.Delete(lessonId), JsonRequestBehavior.DenyGet);
        }
    }
}