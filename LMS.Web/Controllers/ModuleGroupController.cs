﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using LMS.Service.System.ModuleGroup;

namespace LMS.Web.Controllers
{
    public class ModuleGroupController : BaseController
    {
        private readonly IModuleGroupService _moduleGroupService;

        public ModuleGroupController(
            IModuleGroupService moduleGroupService)
        {
            _moduleGroupService = moduleGroupService;
        }

        public JsonResult GetAllModuleGroups()
        {
            return JsonWithContext(_moduleGroupService.GetAllModuleGroups(), JsonRequestBehavior.DenyGet);
        }
    }
}