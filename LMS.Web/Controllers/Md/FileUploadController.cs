﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Md;
using LMS.Service.Md.FileUpload;

namespace LMS.Web.Controllers.Md
{
    public class FileUploadController : BaseController
    {
        private readonly IFileUploadService _service;

        public FileUploadController(
            IFileUploadService service)
        {
            _service = service;
        }

        [HttpPost]
        public JsonResult Upload(FileUploadModel data)
        {
            return JsonWithContext(_service.Upload(data), JsonRequestBehavior.DenyGet);
        }
    }
}