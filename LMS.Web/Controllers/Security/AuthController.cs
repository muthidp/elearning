﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using LMS.Service.Authentication;

namespace LMS.Web.Controllers.Security
{
    public class AuthController : BaseController
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthController(
            IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public JsonResult Login(string email, string password)
        {
            return JsonWithContext(_authenticationService.Authenticate(email, password), JsonRequestBehavior.DenyGet);
        }

        public JsonResult IsLogin()
        {
            return JsonWithContext(_authenticationService.IsLogin(), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Logout()
        {
            _authenticationService.Logout();
            return JsonWithContext(true, JsonRequestBehavior.DenyGet);
        }
    }
}