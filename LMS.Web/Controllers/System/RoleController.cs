﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Sys;
using LMS.Service.System.Role;

namespace LMS.Web.Controllers.System
{
    public class RoleController : BaseController
    {
        private readonly IRoleService _RoleService;

        public RoleController(
            IRoleService RoleService)
        {
            _RoleService = RoleService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var Roles = _RoleService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", Roles},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var Roles = _RoleService.GetAll(institutionId);
            return JsonWithContext(Roles, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(RoleModel Role)
        {
            return JsonWithContext(_RoleService.Save(Role), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long RoleId)
        {
            return JsonWithContext(_RoleService.Delete(RoleId), JsonRequestBehavior.DenyGet);
        }

    }
}