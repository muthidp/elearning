﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Sys;
using LMS.Service.System.User;

namespace LMS.Web.Controllers.System
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(
            IUserService userService)
        {
            _userService = userService;
        }

        public JsonResult GetCurrentUser()
        {
            return JsonWithContext(_userService.GetCurrentUser(), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetUserInfo(string username)
        {
            return JsonWithContext(_userService.GetUserInfo(username), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetUserInstructors(long? institutionId)
        {
            return JsonWithContext(_userService.GetUserInstructors(institutionId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetUserStudents(long? institutionId)
        {
            return JsonWithContext(_userService.GetUserStudents(institutionId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAllForView(long? institutionId, BaseQueryModel queryModel)
        {
            var users = _userService.GetAllWithBaseQuery(institutionId, ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", users},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var users = _userService.GetAll(institutionId);
            return JsonWithContext(users, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(UserModel user)
        {
            return JsonWithContext(_userService.Save(user), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long userId)
        {
            return JsonWithContext(_userService.Delete(userId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult CheckLimit(long InstitutionId)
        {
            return JsonWithContext(_userService.CheckLimit(InstitutionId), JsonRequestBehavior.DenyGet);
        }
    }
}