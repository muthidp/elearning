﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using LMS.Service.System.Institution;
using Core.DTO.Sys;

namespace LMS.Web.Controllers.System
{
    public class InstitutionController : BaseController
    {
        private readonly IInstitutionService _institutionService;

        public InstitutionController(
            IInstitutionService institutionService
            )
        {
            _institutionService = institutionService;
        }

        public JsonResult GetInstitutionByCode(string institutionCode)
        {
            return JsonWithContext(_institutionService.FindOne(institutionCode), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var Institutions = _institutionService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", Institutions},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var Institutions = _institutionService.GetAll(institutionId);
            return JsonWithContext(Institutions, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(InstitutionModel Institution)
        {
            return JsonWithContext(_institutionService.Save(Institution), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long InstitutionId)
        {
            return JsonWithContext(_institutionService.Delete(InstitutionId), JsonRequestBehavior.DenyGet);
        }

    }
}