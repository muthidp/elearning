﻿using System.Web.Mvc;

namespace LMS.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return Redirect("/Web/LMS.angular/index.html");
        }

        public ActionResult Login()
        {
            return Redirect("/Web/login.html");
        }

    }
}
