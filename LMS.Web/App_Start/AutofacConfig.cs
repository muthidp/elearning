﻿using System.ComponentModel.Design;
using Autofac;
using Autofac.Integration.Mvc;
using Core;
using LMS.Service;
using LMS.Service.Lms.Course;

namespace LMS.Web.App_Start
{
    public class AutofacConfig
    {
        public static void Register(ContainerBuilder builder)
        {
            RegisterControllers(builder);
            RegisterServices(builder);
            builder.RegisterFilterProvider();
            builder.RegisterType<ContextProvider>().As<IContextProvider>().InstancePerHttpRequest();
        }

        private static void WireProperties(ContainerBuilder builder)
        {
            //builder.RegisterType<WebApiAuthorizeAttribute>().PropertiesAutowired();
        }

        private static void RegisterControllers(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            Dependency.Register(builder);

            ServiceConfig.RegisterRepositories(builder);
        }
    }
}