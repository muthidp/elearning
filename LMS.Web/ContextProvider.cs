﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core;

namespace LMS.Web
{
    public class ContextProvider : IContextProvider
    {
        public long GetCurrentUserId()
        {
            return long.Parse(HttpContext.Current.User.Identity.Name);
        }

        public bool IsAuthenticated()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }
    }
}