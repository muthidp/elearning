﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.Helper;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class UserSeeder : DataSeeder<Tuple<string, string, string, string, string, bool>, eLearning_Database>
    {
        public override void Set()
        {
            Records.Add(CreateData("admin", "Pass123", "admin@lms.com", "ADMIN", "TU", false));
            Records.Add(CreateData("instructor01", "Pass123", "instructor01@lms.com", "INSTRUCTOR", "TU", false));
            Records.Add(CreateData("student01", "Pass123", "student01@lms.com", "STUDENT", "TU", false));
        }

        public override void Seed(eLearning_Database context)
        {
            var institutions = context.SysInstitutions.Where(e => e.IsActive).ToList();
            var roles = context.SysRoles.Where(e => e.IsActive).ToList();

            Records.ForEach(r =>
            {
                var user = context.SysUsers.FirstOrDefault(u => u.Username.Equals(r.Item1))
                           ?? new sys_user();

                user.Username = r.Item1;
                user.Password = r.Item2.GetMd5Hash();
                user.Email = r.Item3;
                user.IsActive = true;
                user.LastUpdatedBy = Constant.Seeder;
                user.LastUpdatedTime = DateTime.Now;

                var ins = institutions.FirstOrDefault(e => e.InstitutionCode.Equals(r.Item5));
                if (ins != null) user.InstitutionId = ins.InstitutionId;

                var role = roles.FirstOrDefault(e => e.RoleCode.Equals(r.Item4));
                if (role != null) user.RoleId = role.RoleId;

                user.Deactivated = r.Item6;

                if (user.UserId > 0)
                    context.Entry(user).State = EntityState.Modified;
                else
                {
                    user.CreatedBy = Constant.Seeder;
                    user.CreatedTime = DateTime.Now;
                    context.SysUsers.Add(user);
                }

                context.SaveChanges();
            });
        }

        private Tuple<string, string, string, string, string, bool> CreateData(
            string username,
            string password,
            string email,
            string role,
            string institution,
            bool deactivate)
        {
            return new Tuple<string, string, string, string, string, bool>(username, password, email, role, institution, deactivate);
        }
    }
}
