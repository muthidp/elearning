﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class InstitutionSeeder : DataSeeder<Tuple<string, string>, eLearning_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string>("TU", "Telkom University"));
        }

        public override void Seed(eLearning_Database context)
        {
            Records.ForEach(r =>
            {
                var institution = context.SysInstitutions.FirstOrDefault(e => e.InstitutionCode.Equals(r.Item1))
                                  ?? new sys_institution();

                institution.InstitutionCode = r.Item1;
                institution.InstitutionLabel = r.Item2;
                institution.IsActive = true;
                institution.CreatedBy = Constant.Seeder;
                institution.CreatedTime = DateTime.Now;
                institution.LastUpdatedBy = Constant.Seeder;
                institution.LastUpdatedTime = DateTime.Now;

                if(institution.InstitutionId > 0)
                    context.Entry(institution).State = EntityState.Modified;
                else
                    context.SysInstitutions.Add(institution);

                context.SaveChanges();
            });
        }
    }
}
