﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class RoleSeeder : DataSeeder<Tuple<string, string, string>, eLearning_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string, string>("ADMIN", "Instructor", "/instructor/dashboard"));
            Records.Add(new Tuple<string, string, string>("INSTRUCTOR", "Instructor", "/instructor/dashboard"));
            Records.Add(new Tuple<string, string, string>("STUDENT", "Student", "/student/dashboard"));
        }

        public override void Seed(eLearning_Database context)
        {
            Records.ForEach(r =>
            {
                var role = context.SysRoles.FirstOrDefault(e => e.RoleCode.Equals(r.Item1))
                           ?? new sys_role();

                role.RoleCode = r.Item1;
                role.RoleLabel = r.Item2;
                role.DefaultRoute = r.Item3;

                role.IsActive = true;
                role.LastUpdatedBy = Constant.Seeder;
                role.LastUpdatedTime = DateTime.Now;

                if (role.RoleId > 0)
                {
                    context.Entry(role).State = EntityState.Modified;
                }
                else
                {
                    role.CreatedBy = Constant.Seeder;
                    role.CreatedTime = DateTime.Now;
                    context.SysRoles.Add(role);
                }
                context.SaveChanges();
            });
        }
    }
}
