﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class ApplicationSettingSeeder : DataSeeder<Tuple<string, string>, eLearning_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string>("FILEBUCKET", "/Filebucket/"));
        }

        public override void Seed(eLearning_Database context)
        {
            context.ApplicationSettings.RemoveRange(context.ApplicationSettings.ToList());
            context.SaveChanges();

            Records.ForEach(r =>
            {
                var setting = context.ApplicationSettings.FirstOrDefault(e => e.KeyValue.Equals(r.Item1))
                              ?? new sys_application_setting();

                setting.KeyValue = r.Item1;
                setting.SettingValueDefault = r.Item2;
                setting.IsActive = true;

                setting.CreatedBy = "__SEEDER__";
                setting.CreatedTime = DateTime.Now;
                setting.LastUpdatedBy = "__SEEDER__";
                setting.LastUpdatedTime = DateTime.Now;

                if (setting.ApplicationSettingId > 0)
                {
                    context.Entry(setting).State = EntityState.Modified;
                }
                else
                {
                    context.ApplicationSettings.Add(setting);
                }
            });
            context.SaveChanges();
        }
    }
}
