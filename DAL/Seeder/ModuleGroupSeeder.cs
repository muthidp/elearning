﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class ModuleGroupSeeder : DataSeeder<Tuple<string, string, string>, eLearning_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string, string>("Master Data", "", "ACCESS_MASTER_DATA"));
            Records.Add(new Tuple<string, string, string>("Student", "", "ACCESS_STUDENT"));
            Records.Add(new Tuple<string, string, string>("Instructor", "", "ACCESS_INSTRUCTOR"));
            Records.Add(new Tuple<string, string, string>("Courses", "", "ACCESS_COURSES"));
        }

        public override void Seed(eLearning_Database context)
        {
            var acls = context.SysAcls.ToList();

            Records.ForEach(r =>
            {
                var mg = context.SysModuleGroups.FirstOrDefault(e => e.Label.Equals(r.Item1))
                         ?? new sys_module_group();

                mg.Label = r.Item1;
                mg.Route = r.Item2;

                var acl = acls.FirstOrDefault(e => e.Key.Equals(r.Item3));
                if (acl != null)
                    mg.AclId = acl.AclId;

                mg.IsActive = true;
                mg.LastUpdatedBy = Constant.Seeder;
                mg.LastUpdatedTime = DateTime.Now;

                if (mg.ModuleGroupId > 0)
                {
                    context.Entry(mg).State = EntityState.Modified;
                }
                else
                {
                    mg.CreatedBy = Constant.Seeder;
                    mg.CreatedTime = DateTime.Now;
                    context.SysModuleGroups.Add(mg);
                }
            });
            context.SaveChanges();
        }
    }
}
