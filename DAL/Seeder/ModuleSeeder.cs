﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class ModuleSeeder : DataSeeder<Tuple<string, string, string, string>, eLearning_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string, string, string>("Acl", "system/acl", "Master Data", "ACCESS_MASTER_DATA_ACL"));
            Records.Add(new Tuple<string, string, string, string>("Course", "masterData/course", "Master Data", "ACCESS_MASTER_DATA_COURSE"));
            Records.Add(new Tuple<string, string, string, string>("Class", "masterData/class", "Master Data", "ACCESS_MASTER_DATA_CLASS"));
            Records.Add(new Tuple<string, string, string, string>("Lesson", "masterData/lesson", "Master Data", "ACCESS_MASTER_DATA_LESSON"));
        } 

        public override void Seed(eLearning_Database context)
        {
            context.SysModules.ToList().ForEach(e =>
            {
                e.IsActive = false;
                context.Entry(e).State = EntityState.Modified;
            });
            context.SaveChanges();

            var moduleGroups = context.SysModuleGroups.Where(e => e.IsActive).ToList();
            var acls = context.SysAcls.Where(e => e.IsActive).ToList();

            Records.ForEach(r =>
            {
                var module = context.SysModules.FirstOrDefault(e => e.Label.Equals(r.Item1))
                             ?? new sys_module();

                module.Label = r.Item1;
                module.Route = r.Item2;

                var mg = moduleGroups.FirstOrDefault(e => e.Label.Equals(r.Item3));
                if (mg != null)
                    module.ModuleGroupId = mg.ModuleGroupId;

                var acl = acls.FirstOrDefault(e => e.Key.Equals(r.Item4));
                if (acl != null)
                    module.AclId = acl.AclId;

                module.IsActive = true;
                module.LastUpdatedBy = Constant.Seeder;
                module.LastUpdatedTime = DateTime.Now;

                if (module.ModuleId > 0)
                {
                    context.Entry(module).State = EntityState.Modified;
                }
                else
                {
                    module.CreatedBy = Constant.Seeder;
                    module.CreatedTime = DateTime.Now;
                    context.SysModules.Add(module);
                }
                context.SaveChanges();

            });
        }
    }
}
