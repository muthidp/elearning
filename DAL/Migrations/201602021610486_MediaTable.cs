namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MediaTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.lms_lesson", "ClassId", "dbo.lms_class");
            DropIndex("dbo.lms_lesson", new[] { "ClassId" });
            CreateTable(
                "dbo.sys_application_setting",
                c => new
                    {
                        ApplicationSettingId = c.Long(nullable: false, identity: true),
                        KeyValue = c.String(),
                        SettingValueDefault = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ApplicationSettingId);
            
            CreateTable(
                "dbo.md_media_asset",
                c => new
                    {
                        MediaAssetId = c.Long(nullable: false, identity: true),
                        InstitutionId = c.Long(),
                        MediaDescriptorId = c.Long(),
                        Title = c.String(),
                        Description = c.String(),
                        Filename = c.String(),
                        RelativePath = c.String(),
                        AbsolutePath = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.MediaAssetId)
                .ForeignKey("dbo.sys_institution", t => t.InstitutionId)
                .ForeignKey("dbo.md_media_descriptor", t => t.MediaDescriptorId)
                .Index(t => t.InstitutionId)
                .Index(t => t.MediaDescriptorId);
            
            CreateTable(
                "dbo.md_media_descriptor",
                c => new
                    {
                        MediaDescriptorId = c.Long(nullable: false, identity: true),
                        MediaDescriptorLabel = c.String(),
                        MimeTypeDefinition = c.String(),
                        Previewable = c.Boolean(nullable: false),
                        DefaultThumbnailDisplay = c.String(),
                        MediaType = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.MediaDescriptorId);
            
            AlterColumn("dbo.lms_lesson", "ClassId", c => c.Long());
            CreateIndex("dbo.lms_lesson", "ClassId");
            AddForeignKey("dbo.lms_lesson", "ClassId", "dbo.lms_class", "ClassId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.lms_lesson", "ClassId", "dbo.lms_class");
            DropForeignKey("dbo.md_media_asset", "MediaDescriptorId", "dbo.md_media_descriptor");
            DropForeignKey("dbo.md_media_asset", "InstitutionId", "dbo.sys_institution");
            DropIndex("dbo.md_media_asset", new[] { "MediaDescriptorId" });
            DropIndex("dbo.md_media_asset", new[] { "InstitutionId" });
            DropIndex("dbo.lms_lesson", new[] { "ClassId" });
            AlterColumn("dbo.lms_lesson", "ClassId", c => c.Long(nullable: false));
            DropTable("dbo.md_media_descriptor");
            DropTable("dbo.md_media_asset");
            DropTable("dbo.sys_application_setting");
            CreateIndex("dbo.lms_lesson", "ClassId");
            AddForeignKey("dbo.lms_lesson", "ClassId", "dbo.lms_class", "ClassId", cascadeDelete: true);
        }
    }
}
