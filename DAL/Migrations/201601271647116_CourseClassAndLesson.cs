namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CourseClassAndLesson : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.lms_class",
                c => new
                    {
                        ClassId = c.Long(nullable: false, identity: true),
                        ClassCode = c.String(),
                        ClassLabel = c.String(),
                        CourseId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ClassId)
                .ForeignKey("dbo.lms_course", t => t.CourseId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.lms_course",
                c => new
                    {
                        CourseId = c.Long(nullable: false, identity: true),
                        CourseCode = c.String(),
                        CourseLabel = c.String(),
                        Description = c.String(),
                        YearIntake = c.String(),
                        InstitutionId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.CourseId)
                .ForeignKey("dbo.sys_institution", t => t.InstitutionId)
                .Index(t => t.InstitutionId);
            
            CreateTable(
                "dbo.lms_lesson",
                c => new
                    {
                        LessonId = c.Long(nullable: false, identity: true),
                        LessonCode = c.String(),
                        LessonLabel = c.String(),
                        ClassId = c.Long(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.LessonId)
                .ForeignKey("dbo.lms_class", t => t.ClassId, cascadeDelete: true)
                .Index(t => t.ClassId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.lms_lesson", "ClassId", "dbo.lms_class");
            DropForeignKey("dbo.lms_class", "CourseId", "dbo.lms_course");
            DropForeignKey("dbo.lms_course", "InstitutionId", "dbo.sys_institution");
            DropIndex("dbo.lms_lesson", new[] { "ClassId" });
            DropIndex("dbo.lms_course", new[] { "InstitutionId" });
            DropIndex("dbo.lms_class", new[] { "CourseId" });
            DropTable("dbo.lms_lesson");
            DropTable("dbo.lms_course");
            DropTable("dbo.lms_class");
        }
    }
}
