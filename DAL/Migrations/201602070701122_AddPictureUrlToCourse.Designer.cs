// <auto-generated />
namespace DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddPictureUrlToCourse : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddPictureUrlToCourse));
        
        string IMigrationMetadata.Id
        {
            get { return "201602070701122_AddPictureUrlToCourse"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
