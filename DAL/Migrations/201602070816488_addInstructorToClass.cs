namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addInstructorToClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.lms_class", "InstructorId", c => c.Long());
            AddColumn("dbo.lms_class", "CoInstructorId", c => c.Long());
            CreateIndex("dbo.lms_class", "InstructorId");
            CreateIndex("dbo.lms_class", "CoInstructorId");
            AddForeignKey("dbo.lms_class", "CoInstructorId", "dbo.sys_user", "UserId");
            AddForeignKey("dbo.lms_class", "InstructorId", "dbo.sys_user", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.lms_class", "InstructorId", "dbo.sys_user");
            DropForeignKey("dbo.lms_class", "CoInstructorId", "dbo.sys_user");
            DropIndex("dbo.lms_class", new[] { "CoInstructorId" });
            DropIndex("dbo.lms_class", new[] { "InstructorId" });
            DropColumn("dbo.lms_class", "CoInstructorId");
            DropColumn("dbo.lms_class", "InstructorId");
        }
    }
}
