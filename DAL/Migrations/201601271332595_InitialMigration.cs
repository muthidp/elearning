namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.sys_acl",
                c => new
                    {
                        AclId = c.Long(nullable: false, identity: true),
                        Key = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.AclId);
            
            CreateTable(
                "dbo.sys_institution",
                c => new
                    {
                        InstitutionId = c.Long(nullable: false, identity: true),
                        InstitutionCode = c.String(),
                        InstitutionLabel = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.InstitutionId);
            
            CreateTable(
                "dbo.sys_module_group",
                c => new
                    {
                        ModuleGroupId = c.Long(nullable: false, identity: true),
                        Label = c.String(),
                        Route = c.String(),
                        AclId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ModuleGroupId)
                .ForeignKey("dbo.sys_acl", t => t.AclId)
                .Index(t => t.AclId);
            
            CreateTable(
                "dbo.sys_module",
                c => new
                    {
                        ModuleId = c.Long(nullable: false, identity: true),
                        Label = c.String(),
                        Route = c.String(),
                        ModuleGroupId = c.Long(),
                        AclId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ModuleId)
                .ForeignKey("dbo.sys_acl", t => t.AclId)
                .ForeignKey("dbo.sys_module_group", t => t.ModuleGroupId)
                .Index(t => t.ModuleGroupId)
                .Index(t => t.AclId);
            
            CreateTable(
                "dbo.sys_role_acl",
                c => new
                    {
                        RoleAclId = c.Long(nullable: false, identity: true),
                        RoleId = c.Long(),
                        AclId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.RoleAclId)
                .ForeignKey("dbo.sys_acl", t => t.AclId)
                .ForeignKey("dbo.sys_role", t => t.RoleId)
                .Index(t => t.RoleId)
                .Index(t => t.AclId);
            
            CreateTable(
                "dbo.sys_role",
                c => new
                    {
                        RoleId = c.Long(nullable: false, identity: true),
                        RoleCode = c.String(),
                        RoleLabel = c.String(),
                        DefaultRoute = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.sys_user",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 40),
                        Password = c.String(maxLength: 40),
                        Email = c.String(maxLength: 120),
                        RoleId = c.Long(),
                        InstitutionId = c.Long(),
                        Deactivated = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.sys_institution", t => t.InstitutionId)
                .ForeignKey("dbo.sys_role", t => t.RoleId)
                .Index(t => t.Username)
                .Index(t => t.RoleId)
                .Index(t => t.InstitutionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.sys_user", "RoleId", "dbo.sys_role");
            DropForeignKey("dbo.sys_user", "InstitutionId", "dbo.sys_institution");
            DropForeignKey("dbo.sys_role_acl", "RoleId", "dbo.sys_role");
            DropForeignKey("dbo.sys_role_acl", "AclId", "dbo.sys_acl");
            DropForeignKey("dbo.sys_module", "ModuleGroupId", "dbo.sys_module_group");
            DropForeignKey("dbo.sys_module", "AclId", "dbo.sys_acl");
            DropForeignKey("dbo.sys_module_group", "AclId", "dbo.sys_acl");
            DropIndex("dbo.sys_user", new[] { "InstitutionId" });
            DropIndex("dbo.sys_user", new[] { "RoleId" });
            DropIndex("dbo.sys_user", new[] { "Username" });
            DropIndex("dbo.sys_role_acl", new[] { "AclId" });
            DropIndex("dbo.sys_role_acl", new[] { "RoleId" });
            DropIndex("dbo.sys_module", new[] { "AclId" });
            DropIndex("dbo.sys_module", new[] { "ModuleGroupId" });
            DropIndex("dbo.sys_module_group", new[] { "AclId" });
            DropTable("dbo.sys_user");
            DropTable("dbo.sys_role");
            DropTable("dbo.sys_role_acl");
            DropTable("dbo.sys_module");
            DropTable("dbo.sys_module_group");
            DropTable("dbo.sys_institution");
            DropTable("dbo.sys_acl");
        }
    }
}
