namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StudentClass : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.lms_student_class",
                c => new
                    {
                        StudentClassId = c.Long(nullable: false, identity: true),
                        UserId = c.Long(),
                        ClassId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.StudentClassId)
                .ForeignKey("dbo.lms_class", t => t.ClassId)
                .ForeignKey("dbo.sys_user", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ClassId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.lms_student_class", "UserId", "dbo.sys_user");
            DropForeignKey("dbo.lms_student_class", "ClassId", "dbo.lms_class");
            DropIndex("dbo.lms_student_class", new[] { "ClassId" });
            DropIndex("dbo.lms_student_class", new[] { "UserId" });
            DropTable("dbo.lms_student_class");
        }
    }
}
