using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using DAL.Seeder;


namespace DAL.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<eLearning_Database>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private static string GetSeed(string file)
        {
            var baseDir = string.Format("{0}/../../Seeder", AppDomain.CurrentDomain.BaseDirectory);
            return File.ReadAllText(string.Format("{0}/{1}", baseDir, file));
        }

        protected override void Seed(eLearning_Database context)
        {
            /*
            DropObjects(context);
            CreateObjects(context);
            */

            /*
            var sw = new SpWrapper(context);
            sw.ExecuteNonQueryStoredProcedure(new master_acl_data_seeder());
            sw.ExecuteNonQueryStoredProcedure(new master_acl_seeder());
             */

            //new ApplicationSettingSeeder().Execute(context);
            new MediaDescriptorSeeder().Execute(context);

            new InstitutionSeeder().Execute(context);
            new RoleSeeder().Execute(context);
            new AclSeeder().Execute(context);
            new ModuleGroupSeeder().Execute(context);
            new ModuleSeeder().Execute(context);
            new UserSeeder().Execute(context);

        }

        private static void DropObjects(DbContext context)
        {
            //context.Database.ExecuteSqlCommand(new sp_object().TsqlScriptDrop());
        }

        private static void CreateObjects(DbContext context)
        {
           
        }
    }
}
