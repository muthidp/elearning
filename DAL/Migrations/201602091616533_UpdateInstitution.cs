namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateInstitution : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sys_institution", "Subscribed", c => c.Boolean(nullable: false));
            AddColumn("dbo.sys_institution", "UserLimit", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.sys_institution", "UserLimit");
            DropColumn("dbo.sys_institution", "Subscribed");
        }
    }
}
