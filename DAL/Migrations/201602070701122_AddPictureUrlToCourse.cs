namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPictureUrlToCourse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.lms_course", "PictureUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.lms_course", "PictureUrl");
        }
    }
}
