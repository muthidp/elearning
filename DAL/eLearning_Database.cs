﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DAL.Database.Lms;
using DAL.Database.Md;
using DAL.Database.Sys;

namespace DAL
{
    public class eLearning_Database : DbContext
    {
        public eLearning_Database()
            : base("eLearning_Database")
        {

        }

        public DbSet<lms_class> LmsClasses { get; set; }
        public DbSet<lms_course> LmsCourses { get; set; }
        public DbSet<lms_lesson> LmsLessons { get; set; }
        public DbSet<lms_student_class> LmsStudentClasses { get; set; }

        public DbSet<md_media_asset> MdMediaAssets { get; set; }
        public DbSet<md_media_descriptor> MdMediaDescriptors { get; set; }

        public DbSet<sys_acl> SysAcls { get; set; }
        public DbSet<sys_application_setting> ApplicationSettings { get; set; }
        public DbSet<sys_institution> SysInstitutions { get; set; }
        public DbSet<sys_module> SysModules { get; set; }
        public DbSet<sys_module_group> SysModuleGroups { get; set; }
        public DbSet<sys_role> SysRoles { get; set; }
        public DbSet<sys_role_acl> SysRoleAcls { get; set; }
        public DbSet<sys_user> SysUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            System.Data.Entity.Database.SetInitializer<eLearning_Database>(null);
            //base.OnModelCreating(modelBuilder);
        }

    }
}