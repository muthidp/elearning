﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Lms
{
    public class lms_course : BaseEntityModel
    {
        [Key]
        public long CourseId { get; set; }

        public string CourseCode { get; set; }

        public string CourseLabel { get; set; }

        public string Description { get; set; }

        public string YearIntake { get; set; }

        public string PictureUrl { get; set; }

        [ForeignKey("Institution")]
        public long? InstitutionId { get; set; }
        public virtual sys_institution Institution { get; set; }
    }
}
