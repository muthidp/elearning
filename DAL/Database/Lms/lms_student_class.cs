﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Lms
{
    public class lms_student_class : BaseEntityModel
    {
        [Key]
        public long StudentClassId { get; set; }

        [ForeignKey("Student")]
        public long? UserId { get; set; }
        public virtual sys_user Student { get; set; }

        [ForeignKey("Class")]
        public long? ClassId { get; set; }
        public virtual lms_class Class { get; set; }
    }
}
