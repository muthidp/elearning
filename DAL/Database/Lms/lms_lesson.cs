﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Lms
{
    public class lms_lesson : BaseEntityModel
    {
        [Key]
        public long LessonId { get; set; }
 
        public string LessonCode { get; set; }

        public string LessonLabel { get; set; }

        [ForeignKey("Class")]
        public long? ClassId { get; set; }
        public virtual lms_class Class { get; set; }
    }
}
