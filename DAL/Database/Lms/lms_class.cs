﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Lms
{
    public class lms_class : BaseEntityModel
    {
        [Key]
        public long ClassId { get; set; }

        public string ClassCode { get; set; }

        public string ClassLabel { get; set; }

        [ForeignKey("Course")]
        public long? CourseId { get; set; }
        public virtual lms_course Course { get; set; }

        [ForeignKey("Instructor")]
        public long? InstructorId { get; set; }
        public virtual sys_user Instructor { get; set; }

        [ForeignKey("CoInstructor")]
        public long? CoInstructorId { get; set; }
        public virtual sys_user CoInstructor { get; set; }
    }
}
