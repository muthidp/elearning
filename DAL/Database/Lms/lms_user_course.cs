﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Lms
{
    public class lms_user_course : BaseEntityModel
    {
        [Key]
        public long UserCourseId { get; set; }

        [ForeignKey("User")]
        public long? UserId { get; set; }
        public virtual sys_user User { get; set; }

        [ForeignKey("Course")]
        public long? CourseId { get; set; }
        public virtual lms_course Course { get; set; }

    }
}
