﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.Enum;

namespace DAL.Database.Md
{
    public class md_media_descriptor : BaseEntityModel
    {
        [Key]
        public long MediaDescriptorId { get; set; }

        [Display(Name = "Media Descriptor Label")]
        public string MediaDescriptorLabel { get; set; }

        [Display(Name = "Mime Type Definition")]
        public string MimeTypeDefinition { get; set; }

        [Display(Name = "Previewable")]
        public bool Previewable { get; set; }

        [Display(Name = "Default Thumbnail Display")]
        public string DefaultThumbnailDisplay { get; set; }

        [Display(Name = "Media Type")]
        public MediaType MediaType { get; set; }
    }
}
