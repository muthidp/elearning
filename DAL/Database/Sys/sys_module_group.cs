﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Sys
{
    public class sys_module_group : BaseEntityModel
    {
        [Key]
        public long ModuleGroupId { get; set; }

        public string Label { get; set; }

        public string Route { get; set; }

        [ForeignKey("Acl")]
        public long? AclId { get; set; }
        public virtual sys_acl Acl { get; set; }

        public virtual ICollection<sys_module> Modules { get; set; } 
    }
}
