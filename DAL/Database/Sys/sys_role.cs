﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Sys
{
    public class sys_role : BaseEntityModel
    {
        [Key]
        public long RoleId { get; set; }

        public string RoleCode { get; set; }

        public string RoleLabel { get; set; }

        public string DefaultRoute { get; set; }
    }
}
