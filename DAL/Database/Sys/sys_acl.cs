﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Sys
{
    public class sys_acl : BaseEntityModel
    {
        [Key]
        public long AclId { get; set; }

        public string Key { get; set; }

        public string Description { get; set; }
    }
}
