﻿using Autofac;
using Core;

namespace DAL
{
    public class EfAutofacConfig
    {
        public static void Register(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();
            builder.RegisterType<EfUnitOfWork>().As(typeof(IUnitOfWork)).InstancePerRequest();
            builder.RegisterType<eLearning_Database>();
        }
    }
}
